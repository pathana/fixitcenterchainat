<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::middleware('jwt.auth')->get('users', function() {
  return auth('api')->user();
});

Route::group([
  'middleware' => 'api',
  'prefix' => 'auth'
], function ($router) {
      Route::post('authenticate', 'AuthController@authenticate');
      Route::post('login', 'AuthController@login');
      Route::post('logout', 'AuthController@logout');
      Route::post('refresh', 'AuthController@refresh');
      Route::post('user', 'AuthController@user');
});

Route::apiResource('users', 'UserController');

Route::apiResource('richtexts', 'RichtextController');

Route::apiResource('posts', 'PostController');

Route::apiResource('advertises', 'AdvertiseController');

Route::apiResource('files', 'FileController');

Route::apiResource('citizens', 'CitizenController');

Route::apiResource('centers', 'CenterController');

Route::apiResource('repairs', 'RepairController');

Route::apiResource('items', 'ItemController');

Route::apiResource('officers', 'OfficerController');

Route::apiResource('departments', 'DepartmentController');

Route::apiResource('spare_parts', 'SparePartController');

Route::apiResource('inventories', 'InventoryController');

Route::apiResource('item_types', 'ItemTypeController');

Route::apiResource('builds', 'BuildController');

Route::apiResource('peoples', 'PeopleController');

Route::get('repair_maincenter', 'RepairController@maincenter');

Route::get('citizen_search', 'CitizenController@search');
Route::get('citizens/{citizen}/repairs', 'CitizenController@repairs');
Route::post('citizens/store_api', 'CitizenController@store_api');

Route::get('officer_search', 'OfficerController@search');
Route::get('officer_all', 'OfficerController@all');

Route::get('item_suggestion', 'ItemController@suggestion');
Route::get('item_suggestions', 'ItemController@suggestions');

Route::get('repairs/center/{id}', 'RepairController@center');

Route::get('repairs/waitrepair/{center_id?}', 'RepairController@wait_repair');
Route::get('repairs/waitsendback/{center_id?}', 'RepairController@wait_sendback');
Route::get('repairs/finished/{center_id?}', 'RepairController@finished');
Route::get('repairs/notwait/{center_id?}/{date}', 'RepairController@not_wait');

Route::post('departments/aka', 'DepartmentController@aka');

Route::get('spare_parts/department/{id?}', 'SparePartController@department');
Route::get('spare_parts/categories/{id?}', 'SparePartController@categories');

Route::get('inventories/last/{part_id}', 'InventoryController@last');
Route::get('inventories/part/{part_id}', 'InventoryController@part');
Route::post('inventories/upload', 'InventoryController@upload');

Route::put('items/{item}/item_return', 'ItemController@item_return');

Route::get('centers/{center}/report', 'CenterController@report');
