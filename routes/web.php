<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();

Route::get('logout', 'UserController@logout');

Route::resource('ratings', 'RatingController');

Route::get('mission', 'LandingController@mission')->name('mission');

Route::get('personnel', 'LandingController@personnel')->name('personnel');

Route::get('rating', 'LandingController@rating')->name('rating');

Route::get('/', 'LandingController@index');

Route::get('advertises/{advertise}', 'LandingController@advertiseSingle');

Route::get('post/{post}', 'LandingController@postSingle');

Route::any('allpost', 'LandingController@postAll');

Route::any('advertises', 'LandingController@advertises');

Route::get('download/{id}', 'LandingController@download')->name('downloadDoc');

Route::get('/home', 'HomeController@index')->name('home');
// Route::get('/', 'PostController@all');
Route::get('/posts/{post}', 'PostController@single');

Route::get('/admin/{any}', 'AdminController@index')->where('any', '.*');

Route::get('/center/{any}', 'AdminController@center')->where('any', '.*');

Route::get('/officer/{any}', 'AdminController@officer')->where('any', '.*');

Route::get('/inventories/{any}', 'AdminController@inventories')->where('any', '.*');

Route::get('pdf', 'LandingController@pdf');

Route::get('repairs/printable/{id}', 'RepairController@printable');

Route::any('finditem', 'LandingController@itemFind');
