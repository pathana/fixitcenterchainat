<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Fixitcenter chaiant - @yield('title')</title>

    <style type="text/css">
      div.card-left {
        width: 90mm;
        height: 60mm;
        max-height: 60mm;
        border: 1px solid black;
        float: left;
      }

      div.card-right {
        width: 90mm;
        height: 60mm;
        max-height: 60mm;
        border: 1px solid black;
        float: right;
      }

      div.space {
        margin-top: 10mm;
      }

      p {
        margin: 0in 0in 0.0001pt;
        line-height: normal;
        font-size: 11pt;
        padding-left: 5mm;
      }

      table {
        padding-left: 5mm;
      }

      strong {
        color: blue;
      }

      body {
        font-family: 'thsarabun', sans-serif;
      }

      img.item {
        max-width: 100px;
        max-height: 100px;
        float: right;
        margin-right: 5mm;
      }

      img.left {
        max-width: 100px;
        max-height: 100px;
        float: left;
        margin-right: 5mm;
      }

      .page_break { page-break-before: always; }

      @page {
        margin-left: 10mm;
        margin-top: 10mm;
      }
    </style>
</head>
<body>
  <div class="card-left">
    <p style="font-size: 6pt;">&nbsp;</p>
    <p><strong>{{ $repair->center->name}}</strong></p>
    <p><strong>เลขที่ใบรับซ่อม </strong> {{ $repair->id }} <strong>วันที่ </strong> {{ $repair->created_at}}</p>
    <p><strong>{{ $repair->citizen->title . ' ' . $repair->citizen->fullname}} เบอร์โทรศัพ {{ $repair->citizen->phoneFormat}}</strong></p>
    <p><strong>สิ่งของที่นำมาซ่อม </strong> {{ $repair->item->item_type->name }} </p>
    <p><strong>อาการเสีย</strong> {{ $repair->item->problem}}</p>
    <p><strong>รายละเอียด</strong> {{ $repair->item->description}}</p>
    <p><strong>ผลการซ่อม ซ่อมได้ &#9634; ซ่อมไม่ได้ &#9634; ส่งซ่อมต่อ &#9634;</strong>....................................</p>
    <p><strong>ผู้ซ่อม</strong>........................................................................................</p>
    <p><strong>วันที่ซ่อมเสร็จ </strong>.................................................................................</p>
  </div>
  <div class="card-right">
    <p style="font-size: 6pt;">&nbsp;</p>
    <p><strong>{{ $repair->center->name}}</strong></p>
    <p><strong>เลขที่ใบรับซ่อม </strong> {{ $repair->id }} <strong>วันที่ </strong> {{ $repair->created_at}}</p>
    <p><strong>{{ $repair->citizen->title . ' ' . $repair->citizen->fullname}} เบอร์โทรศัพ {{ $repair->citizen->phoneFormat}}</strong></p>
    <p><strong>สิ่งของที่นำมาซ่อม </strong> {{ $repair->item->item_type->name }} </p>
    <p><strong>อาการเสีย</strong> {{ $repair->item->problem}}</p>
    <p><strong>รายละเอียด</strong> {{ $repair->item->description}}</p>
    {{-- <p><strong>รายละเอียด</strong> {{ $repair->item->description}}</p> --}}
    {{-- @if (!empty($repair->item->flies->first()->destination))
      @php
        $path = public_path('upload/items') . '/' . $repair->item->flies->first()->destination;
      @endphp
      <img class="item" src="{{ $path }}"/>
    @endif --}}
    <div class="visible-print">
      @php
        $path = public_path('upload/qrcode') . '/' . $repair->item->id;
      @endphp
      <table>
        <tr>
          <td>
            <p><strong>ติดต่อสอบถาม 084-1499694 (อ.ชนันท์วัลย์)</strong></p>
            <p>ตรวจสอบสถานะของซ่อม สแกน qrcode</p>
          </td>
          <td>
            <img class="item" src="{{ $path }}"/>
          </td>
        </tr>
      </table>

      {{-- <img class="item" src="{{ $path }}"/> --}}
      {{-- {!! QrCode::encoding('UTF-8')->size(100)->generate('fixit.com'); !!} --}}
      {{-- {!! QrCode::format('png')->size(100)->generate('fixit.com'); !!} --}}
      {{-- <img class="item" src="{!! QrCode::format('png')->size(100)->generate('fixit.com'); !!}" --}}
    </div>
  </div>
</body>
</html>
