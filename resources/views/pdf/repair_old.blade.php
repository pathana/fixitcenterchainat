<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    {{-- <title>{{ config('app.name', 'Laravel') }}</title> --}}
    <title>Fixitcenter chaiant - @yield('title')</title>

    <!-- Styles -->
    <link href="{{ asset('css/landing.css') }}" rel="stylesheet">

    <style lang="css">
      .card-body > p {
        line-height: 1;
        font-size: 10pt;
        z-index: 1;
      }

      .card {
        border: 0px;
        background-color: transparent;
        min-height: 300px;
        max-height: 300px;
      }

      img.bg {
        max-width: 150px;
        max-height: 150px;
        z-index: -1;
        opacity: 0.3;
        position: absolute;
        left: 45mm;
        top: 50px;
      }

      img.item {
        max-width: 150px;
        max-height: 150px;
        z-index: 0;
        position: absolute;
        right: 10mm;
        bottom: 5mm;
      }

      strong {
        color: blue;
      }
    </style>
</head>
<body>
  <div class="container">
    <div class="row">
      <div class="col-md-6 col-xs-6 border border-dark">
        <div class="card">
          <div class="card-body">
            <img class="bg" src="http://www.fixitcenter.org/fixit62/imgschool/null_img.png" />
            <p><strong>{{ $repair->center->name}} เลขที่ใบรับซ่อม </strong> {{ $repair->id }} <strong>วันที่ </strong> {{ $repair->created_at}}</p>
            <p><strong>{{ $repair->citizen->title . ' ' . $repair->citizen->fullname}} เบอร์โทรศัพ {{ $repair->citizen->phoneFormat}}</strong></p>
            <p><strong>สิ่งของที่นำมาซ่อม </strong> {{ $repair->item->name }} <strong>ประเภท</strong> {{ $repair->item->type_name }}</p>
            <p style="line-height: 1;"><strong>อาการเสีย</strong> {{ $repair->item->problem}}</p>
            <p style="line-height: 1;"><strong>สภาพสินค้า</strong> {{ $repair->item->status}}</p>
            <p style="line-height: 1;"><strong>อุปกรณ์ที่ติดมาด้วย</strong> {{ $repair->item->attach}}</p>
            <p><strong>ผลการซ่อม ซ่อมได้ &#9634; ซ่อมไม่ได้ &#9634; ส่งซ่อมต่อ &#9634;</strong>....................................</p>
            <p><strong>ผู้ซ่อม</strong>........................................................................................</p>
            <p><strong>วันที่ซ่อมเสร็จ </strong>.................................................................................</p>
          </div>
        </div>
      </div>
      <div class="col-md-6 col-xs-6 border border-dark">
        <div class="card">
          <div class="card-body">
            <img class="bg" src="http://www.fixitcenter.org/fixit62/imgschool/null_img.png" />
            <p><strong>{{ $repair->center->name}} เลขที่ใบรับซ่อม </strong> {{ $repair->id }} <strong>วันที่ </strong> {{ $repair->created_at}}</p>
            <p><strong>{{ $repair->citizen->title . ' ' . $repair->citizen->fullname}} เบอร์โทรศัพ {{ $repair->citizen->phoneFormat}}</strong></p>
            <p><strong>สิ่งของที่นำมาซ่อม </strong> {{ $repair->item->name }} <strong>ประเภท</strong> {{ $repair->item->type_name }}</p>
            <p style="line-height: 1;"><strong>อาการเสีย</strong> {{ $repair->item->problem}}</p>
            <p style="line-height: 1;"><strong>สภาพสินค้า</strong> {{ $repair->item->status}}</p>
            <p style="line-height: 1;"><strong>อุปกรณ์ที่ติดมาด้วย</strong> {{ $repair->item->attach}}</p>
            @if (!empty($repair->item->flies->first()->destination))
              <img class="item" src="{!! '/upload/items/' . $repair->item->flies->first()->destination !!}" />
            @endif
          </div>
        </div>
      </div>
    </div>

  </div>
  <script src="{{ asset('js/app.js')}}"></script>
</body>
</html>
