<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Fixitcenter chaiant - @yield('title')</title>

    <style type="text/css">
      div.card {
        width: 80mm;
        height: 40mm;
        max-height: 40mm;
        border: 1px solid black;
        float: left;
        z-index: 1;
      }

      div.card-2 {
        width: 80mm;
        height: 40mm;
        max-height: 40mm;
        border: 1px solid black;
        float: left;
        z-index: 1;
        /* margin-top: 5mm; */
        /* padding-top: 5mm; */
      }

      div.space {
        margin-top: 5mm;
      }

      p {
        margin: 0in 0in 0.0001pt;
        line-height: normal;
        font-size: 11pt;
        padding-left: 5mm;
      }

      strong {
        color: blue;
      }

      body {
        font-family: 'thsarabun', sans-serif;
      }

      img.item {
        max-width: 100px;
        max-height: 100px;
        float: right;
        margin-right: 5mm;
      }

      .page_break { page-break-before: always; }

      @page {
        margin-left: 10mm;
        margin-top: 0mm;
      }
    </style>
</head>
<body>
  <div class="card-2">
    <p><strong>{{ $repair->center->name}}</strong></p>
    <p><strong>เลขที่ใบรับซ่อม </strong> {{ $repair->id }} <strong>วันที่ </strong> {{ $repair->created_at}}</p>
    <p><strong>{{ $repair->citizen->title . ' ' . $repair->citizen->fullname}} {{ $repair->citizen->phoneFormat}}</strong></p>
    <p><strong>สิ่งของที่นำมาซ่อม </strong> {{ $repair->item->item_type->name }} </p>
    <p><strong>อาการเสีย</strong> {{ $repair->item->problem}}</p>
    <p><strong>รายละเอียด</strong> {{ $repair->item->description}}</p>
    @php
      $path = public_path('upload/qrcode') . '/' . $repair->item->id;
    @endphp
    <p><strong>ติดต่อสอบถาม 084-1499694 (อ.ชนันท์วัลย์)</strong></p>
    <p>ตรวจสอบสถานะของซ่อม สแกน qrcode</p>
    <img class="item" src="{{ $path }}" / />
  </div>
  {{-- <div class="space"></div> --}}
  <div class="page_break"></div>
  <div class="card">
    <p><strong>{{ $repair->center->name}}</strong></p>
    <p><strong>เลขที่ใบรับซ่อม </strong> {{ $repair->id }} <strong>วันที่ </strong> {{ $repair->created_at}}</p>
    <p><strong>{{ $repair->citizen->title . ' ' . $repair->citizen->fullname}} {{ $repair->citizen->phoneFormat}}</strong></p>
    <p><strong>สิ่งของที่นำมาซ่อม </strong> {{ $repair->item->item_type->name }} </p>
    <p><strong>อาการเสีย</strong> {{ $repair->item->problem}}</p>
    <p><strong>รายละเอียด</strong> {{ $repair->item->description}}</p>
    <p><strong>ผลการซ่อม ซ่อมได้ &#9634; ซ่อมไม่ได้ &#9634; ส่งซ่อมต่อ &#9634;</strong></p>
  </div>
</body>
</html>
