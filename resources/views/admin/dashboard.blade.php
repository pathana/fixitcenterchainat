 <!DOCTYPE html>
 <html lang="en">
 <head>
     <meta charset="UTF-8">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <meta http-equiv="X-UA-Compatible" content="ie=edge">
     <meta name="csrf-token" content="{{ csrf_token() }}">
     <script> window.Laravel = { csrfToken: 'cssrf_token()'}</script>
     <title> Welcome to the Admin dashboard </title>
     <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">

    <link href="{{ asset('css/admindashboard.css') }}" rel="stylesheet">
 </head>
 <body id="page-top" class="sidebar-toggled">
    {{-- Vue Component --}}
    <div id="admin">
      <Homepage
        :user-name='@json(auth()->user()->name)'
        :user-id='@json(auth()->user()->id)'
        :user-avatar='@json(auth()->user()->avatar)'
      ></Homepage>
    </div>
    {{-- End of Vue Component --}}

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <script src="{{ asset('js/admin.js')}}"></script>

</body>

</html>
