@extends('layouts.landing')

@section('title', 'ความพึงพอใจ')

@section('content')
  @php
    $sex_array = array("1" => "ชาย", "2" => "หญิง");
    $age_array = array(
      "1" => "ต่ำกว่า 18 ปี", "2" => "ระหว่าง 18-23 ปี",
      "3" => "ระหว่าง 24-28 ปี", "4" => "ระหว่าง 29-33 ปี", "5" => "มากกว่า 33 ปี"
    );
    $career_array = array(
      "1" => "เกษตรกร", "2" => "ค้าขาย",
      "3" => "รับจ้างทั่วไป", "4" => "รับราชการ/พนักงานของรัฐ",
      "5" => "พนักงานรัฐวิสาหกิจ", "6" => "พนักงานบริษัท",
      "7" => "อื่นๆ"
    );
    $education_array = array(
      "1" => "ต่ำกว่า ม.3", "2" => "ม.3",
      "3" => "ม.6 / ปวช.", "4" => "ปวส.",
      "5" => "ปริญญาตรี", "6" => "สูงกว่าปริญญาตรี"
    );
    $score_array = array(
      "4" => "มาก", "3" => "ปานกลาง", "2" => "น้อย", "1" => "ปรับปรุง"
    );
    $options_array = array(
      "option1" => "1. ความรู้ /ความสามารถของผู้ให้บริการ",
      "option2" => "2. ความพร้อมของเครื่องมือ/อุปกรณ์ในการให้บริการ",
      "option3" => "3. ระยะเวลาในการให้บริการ",
      "option4" => "4. ประโยชน์ที่ได้รับจากการให้บริการ",
      "option5" => "5. ความสะดวกในการเข้ารับบริการ",
      "option6" => "6. ผู้ให้บริการสามารถนำความรู้ที่ได้รับไปประกอบอาชีพได้",
      "option7" => "7. ผู้ให้บริการมีกริยา วาจา และมารยาทสุภาพ",
      "summary" => "รวม"
    );
  @endphp

  <div class="row mt-3 pb-3">
    <div class="col"></div>
    <div class="col-lg-9 col-sm-10">
      <h5>ตาราง แสดงจำนวนและร้อยละของผู้ตอบแบบสอบถาม</h5>
      <table class="table table-bordered">
        <thead>
          <tr>
            <th scope="col">รายการ</th>
            <th scope="col">จำนวน</th>
            <th scope="col">ร้อยละ</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <th colspan="3">เพศ</th>
          </tr>
          @foreach ($sex_array as $key => $value)
            <tr>
              <td>{{$value}}</td>
              <td>{{$sexArray[$key]['count']}}</td>
              <td>{{$sexArray[$key]['percent']}}</td>
            </tr>
          @endforeach

          <tr>
            <th colspan="3">อายุ</th>
          </tr>
          @foreach ($age_array as $key => $value)
            <tr>
              <td>{{$value}}</td>
              <td>{{$ageArray[$key]['count']}}</td>
              <td>{{$ageArray[$key]['percent']}}</td>
            </tr>
          @endforeach

          <tr>
            <th colspan="3">อาชีพ</th>
          </tr>
          @foreach ($career_array as $key => $value)
            <tr>
              <td>{{$value}}</td>
              <td>{{$careerArray[$key]['count']}}</td>
              <td>{{$careerArray[$key]['percent']}}</td>
            </tr>
          @endforeach

          <tr>
            <th colspan="3">ระดับการศึกษา</th>
          </tr>
          @foreach ($education_array as $key => $value)
            <tr>
              <td>{{$value}}</td>
              <td>{{$educationArray[$key]['count']}}</td>
              <td>{{$educationArray[$key]['percent']}}</td>
            </tr>
          @endforeach

          <tr>
            <th>รวม</th>
            <td>{{ $peoples }}</td>
            <td>100.0</td>
          </tr>
        </tbody>
      </table>

      <h5>ตาราง ค่าเฉลี่ยด้านความพึงพอใจ</h5>
      <table class="table table-bordered">
        <thead>
          <th scope="col">รายการ</th>
          <th scope="col">ค่าเฉลี่ย</th>
          <th scope="col">ระดับความพึงพอใจ</th>
        </thead>
        <tbody>
          @foreach ($options_array as $key => $value)
            <tr>
              <td>{{ $value }}</td>
              <td>{{ $ratingStats[$key]['avg']}}</td>
              <td>{{ $ratingStats[$key]['description']}}</td>
            </tr>
          @endforeach

        </tbody>

      </table>

    </div>
    <div class="col"></div>

  </div>

@endsection

@section('footer-class')
  <footer class="footer bg-light">
@endsection
