@extends('layouts.landing')

@section('title', 'ความพึงพอใจ')

@section('content')
  @php
    $sex_array = array("1" => "ชาย", "2" => "หญิง");
    $age_array = array(
      "1" => "ต่ำกว่า 18 ปี", "2" => "ระหว่าง 18-23 ปี",
      "3" => "ระหว่าง 24-28 ปี", "4" => "ระหว่าง 29-33 ปี", "5" => "มากกว่า 33 ปี"
    );
    $career_array = array(
      "1" => "เกษตรกร", "2" => "ค้าขาย",
      "3" => "รับจ้างทั่วไป", "4" => "รับราชการ/พนักงานของรัฐ",
      "5" => "พนักงานรัฐวิสาหกิจ", "6" => "พนักงานบริษัท",
      "7" => "อื่นๆ"
    );
    $education_array = array(
      "1" => "ต่ำกว่า ม.3", "2" => "ม.3",
      "3" => "ม.6 / ปวช.", "4" => "ปวส.",
      "5" => "ปริญญาตรี", "6" => "สูงกว่าปริญญาตรี"
    );
    $score_array = array(
      "5" => "มากที่สุด",
      "4" => "มาก",
      "3" => "ปานกลาง",
      "2" => "น้อย",
      "1" => "น้อยที่สุด"
    );
    $options_array = array(
      "option1" => "1. ความรู้ /ความสามารถของผู้ให้บริการ",
      "option2" => "2. ความพร้อมของเครื่องมือ/อุปกรณ์ในการให้บริการ",
      "option3" => "3. ระยะเวลาในการให้บริการ",
      "option4" => "4. ประโยชน์ที่ได้รับจากการให้บริการ",
      "option5" => "5. ความสะดวกในการเข้ารับบริการ",
      "option6" => "6. ผู้ให้บริการสามารถนำความรู้ที่ได้รับไปประกอบอาชีพได้",
      "option7" => "7. ผู้ให้บริการมีกริยา วาจา และมารยาทสุภาพ"
    );
  @endphp
  <div class="row mt-3 pb-3">
    <div class="col"></div>
    <div class="col-lg-9 col-sm-10">
      @if ($errors->any())
        @foreach ($errors->all() as $error)
          <div class="alert alert-danger" role="alert">{{ $error }}</div>
        @endforeach
      @endif
      <h4 class="text-center">แบบประเมินความพึงพอใจการดำเนินงาน</h4>
      <h4 class="text-center">ศูนย์ซ่อมสร้างเพื่อชุมชน (Fix it Center) ศูนย์ถาวร</h4>
      <form action="/ratings" method="post">
        {{ csrf_field() }}
        <div class="form-gorup">
          <label>ตอนที่ 1 สภาพทั่วไปของผู้ตอบแบบประเมิน</label>
        </div>
        <div class="form-group row">
          <label class="col-sm-3 col-form-label">เพศ</label>
          <div class="col-sm-9">
            @foreach ($sex_array as $key => $value)
              <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="sex" value={{$key}} />
                <label class="form-check-label">{{$value}}</label>
              </div>
            @endforeach
          </div>
        </div>
        <div class="form-group row">
          <label class="col-sm-3 col-form-label">อายุ</label>
          <div class="col-sm-9">
            @foreach ($age_array as $key => $value)
              <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="age" value={{$key}} />
                <label class="form-check-label">{{$value}}</label>
              </div>
            @endforeach
          </div>
        </div>
        <div class="form-group row">
          <label class="col-sm-3 col-form-label">อาชีพ</label>
          <div class="col-sm-9">
            @foreach ($career_array as $key => $value)
              <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="career" value={{$key}} />
                <label class="form-check-label">{{$value}}</label>
              </div>
            @endforeach
          </div>
        </div>
        <div class="form-group row">
          <label class="col-sm-3 col-form-label">ระดับการศึกษา</label>
          <div class="col-sm-9">
            @foreach ($education_array as $key => $value)
              <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="education" value={{$key}} />
                <label class="form-check-label">{{$value}}</label>
              </div>
            @endforeach
          </div>
        </div>

        <div class="form-gorup">
          <label>ตอนที่ 2 แบบประเมินความพึงพอใจในการรับบริการ</label>
        </div>

        <table class="table">
          <thead>
            <tr>
              <th scope="col">รายการ</th>
              <th scope="col">มากที่สุด</th>
              <th scope="col">มาก</th>
              <th scope="col">ปานกลาง</th>
              <th scope="col">น้อย</th>
              <th scope="col">น้อยที่สุด</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($options_array as $key => $value)
              <tr>
                <th scope="row">{{$value}}</th>
                @for ($i = 5; $i > 0; $i--)
                  <td>
                    <input type="radio" name={{$key}} value={{$i}} />
                  </td>
                @endfor
              </tr>
            @endforeach
          </tbody>
        </table>

        <div class="form-group text-center">
          <button type="submit" class="btn btn-primary">ตกลง</button>
        </div>
      </form>
    </div>
    <div class="col"></div>
  </div>
@endsection

@section('footer-class')
  <footer class="footer bg-light">
@endsection
