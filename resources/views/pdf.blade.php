<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    {{-- <title>{{ config('app.name', 'Laravel') }}</title> --}}
    <title>Fixitcenter chaiant - @yield('title')</title>

    <!-- Styles -->
    {{-- <link href="{{ asset('css/landing.css') }}" rel="stylesheet"> --}}

    <style lang="css">
      div.card {
        display: inline-block;
        width: 90mm;
        min-height: 60mm;
        max-height: 60mm;
        border: 1px solid black;
      }

      div > span {
        font-size: 10pt;
      }

      div > p {
        font-size: 8pt;
        margin-top: 0;
        margin-left: 5mm;
        line-height: 1;
      }
    </style>
</head>
<body>
  <div class="card">
    <p class="strong"></p>
    <p><strong>{{ $repair->center->name}} เลขที่ใบรับซ่อม </strong> {{ $repair->id }} <strong>วันที่ </strong> {{ $repair->created_at}}</p>
    <p><strong>{{ $repair->citizen->title . ' ' . $repair->citizen->fullname}} เบอร์โทรศัพ {{ $repair->citizen->phoneFormat}}</strong></p>
    <p><strong>สิ่งของที่นำมาซ่อม </strong> {{ $repair->item->name }} <strong>ประเภท</strong> {{ $repair->item->type_name }}</p>
    <p><strong>อาการเสีย</strong> {{ $repair->item->problem}}</p>
    <p><strong>สภาพสินค้า</strong> {{ $repair->item->status}}</p>
    <p><strong>อุปกรณ์ที่ติดมาด้วย</strong> {{ $repair->item->attach}}</p>
    <p><strong>ผลการซ่อม ซ่อมได้ &#9634; ซ่อมไม่ได้ &#9634; ส่งซ่อมต่อ &#9634;</strong>....................................</p>
    <p><strong>ผู้ซ่อม</strong>........................................................................................</p>
    <p><strong>วันที่ซ่อมเสร็จ </strong>.................................................................................</p>
  </div>
  <div class="card">
    <p class="strong"></p>
    <p><strong>{{ $repair->center->name}} เลขที่ใบรับซ่อม </strong> {{ $repair->id }} <strong>วันที่ </strong> {{ $repair->created_at}}</p>
    <p><strong>{{ $repair->citizen->title . ' ' . $repair->citizen->fullname}} เบอร์โทรศัพ {{ $repair->citizen->phoneFormat}}</strong></p>
    <p><strong>สิ่งของที่นำมาซ่อม </strong> {{ $repair->item->name }} <strong>ประเภท</strong> {{ $repair->item->type_name }}</p>
    <p><strong>อาการเสีย</strong> {{ $repair->item->problem}}</p>
    <p><strong>สภาพสินค้า</strong> {{ $repair->item->status}}</p>
    <p><strong>อุปกรณ์ที่ติดมาด้วย</strong> {{ $repair->item->attach}}</p>
    <p><strong>ผลการซ่อม ซ่อมได้ &#9634; ซ่อมไม่ได้ &#9634; ส่งซ่อมต่อ &#9634;</strong>....................................</p>
    <p><strong>ผู้ซ่อม</strong>........................................................................................</p>
    <p><strong>วันที่ซ่อมเสร็จ </strong>.................................................................................</p>
  </div>
  {{-- <div class="container">
    <div class="row">
      <div class="col-md-6 col-xs-6 border border-dark">
        <div class="card">
          <div class="card-body">
            <img class="bg" src="http://www.fixitcenter.org/fixit62/imgschool/null_img.png" />
            <p><strong>ศูนย์ถาวร เลขที่ใบรับซ่อม </strong> 54 <strong>วันที่ </strong> 11 เมษยน 2562</p>
            <p><strong>นาย พํฒนะ ุภูดิสโสภณ เบอร์โทรศัพ 084-1499694</strong></p>
            <p><strong>สิ่งของที่นำมาซ่อม </strong> พัดลม <strong>ประเภท</strong> เครื่องใช้ไฟฟ้า/เครื่องใช้ในครัวเรือน</p>
            <p style="line-height: 1;"><strong>อาการเสีย</strong></p>
            <p style="line-height: 1;"><strong>สภาพสินค้า</strong></p>
            <p style="line-height: 1;"><strong>อุปกรณ์ที่ติดมาด้วย</strong></p>
            <p><strong>ผลการซ่อม ซ่อมได้ &#9634; ซ่อมไม่ได้ &#9634; ส่งซ่อมต่อ &#9634;</strong>....................................</p>
            <p><strong>ผู้ซ่อม</strong>........................................................................................</p>
            <p><strong>วันที่ซ่อมเสร็จ </strong>.................................................................................</p>
          </div>
        </div>
      </div>
      <div class="col-md-6 col-xs-6 border border-dark">
        <div class="card">
          <div class="card-body">
            <img class="bg" src="http://www.fixitcenter.org/fixit62/imgschool/null_img.png" />
            <p><strong>ศูนย์ถาวร เลขที่ใบรับซ่อม </strong> 54 <strong>วันที่ </strong> 11 เมษยน 2562</p>
            <p><strong>นาย พํฒนะ ุภูดิสโสภณ เบอร์โทรศัพ 084-1499694</strong></p>
            <p><strong>สิ่งของที่นำมาซ่อม </strong> พัดลม <strong>ประเภท</strong> เครื่องใช้ไฟฟ้า/เครื่องใช้ในครัวเรือน</p>
            <p style="line-height: 1;"><strong>อาการเสีย</strong></p>
            <p style="line-height: 1;"><strong>สภาพสินค้า</strong></p>
            <p style="line-height: 1;"><strong>อุปกรณ์ที่ติดมาด้วย</strong></p>
            <img class="item" src="https://static.bigc.co.th/media/catalog/product/cache/2/image/497x497/9df78eab33525d08d6e5fb8d27136e95/8/8/8850918004322_1.jpg" />
          </div>
        </div>
      </div>
    </div>

  </div> --}}
  {{-- <script src="{{ asset('js/app.js')}}"></script> --}}
</body>
</html>
