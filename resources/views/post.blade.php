@extends('layouts.landing')

@section('title', 'ข่าวประชาสัมพันธ์')

@section('content')
  <div class="row mt-3 pb-3">
    <div class="col-lg-2"></div>
    <div class="col-lg-8">
      <div class="card">
        <div class="card-header text-center">
          {{ $post->type_name }}
        </div>
        <div class="card-body">
          <h6>หัวข้อข่าว : <small class="text-muted"> {{ $post->title}}</small></h6>
          <h6 class="mt-3">รายละเอียดข่าว</h6>
          <div class="ml-3">
            {!! $post->description !!}
          </div>
          <h6 class="mt-3">รูปภาพ</h6>
          <div class="row">
            @foreach ($post->flies()->photo()->get() as $photo)
              <div class="col-lg-3 col-md-3 col-4 pb-2">
                @desktop
                  <img src="{!! '/upload/posts/'.$photo->destination !!}" width="200" height="200" class="rounded border border-dark" />
                @elsedesktop
                  <img src="{!! '/upload/posts/'.$photo->destination !!}" width="100" height="100" class="rounded border border-dark" />
                @enddesktop
              </div>
            @endforeach
          </div>

          <h6 class="mt-3">เอกสาร</h6>
          <div class="row">
            <div class="col-12">
              <table class="table mt-3">
                <thead>
                  <th scope="col">#</th>
                  <th scope="col">name</th>
                  <th scope="col">download</th>
                </thead>
                <tbody>
                  @foreach ($post->flies()->file()->get() as $doc)
                    <tr>
                      <td>{{ $doc->id }}</td>
                      <td>{{ $doc->name }}</td>
                      <td><a href="{{ route('downloadDoc', $doc->id)}}" target="_blank">Download</a></td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>
@endsection

@section('footer-class')
    <footer class="footer bg-light">
@endsection
