@extends('layouts.landing')

@section('title', 'หน้าที่และความรับผิดชอบ')

@section('content')
  <div class="row mt-3 pb-3">
    <div class="col-lg-2"></div>
    <div class="col-lg-8">
      <div class="card shadow">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary">หน้าที่และความรับผิดชอบ</h6>
        </div>
        <div class="card-body">
          {!! $mission->content !!}
        </div>
      </div>
    </div>
  </div>
@endsection

@section('footer-class')
  <footer class="footer bg-light fixed-bottom">
@endsection
