@extends('layouts.landing')

@section('title', 'ของซ่อม')

@section('content')
  <div class="row mt-3 pb-3">
    <div class="col"></div>
    <div class="col-12 col-lg-3">
      <div class="card">
        <div class="card-body">
          <h5 class="card-title">บริการข้อมูลของซ่อม</h5>
          <h6 class="card-title">ชื่อศูนย์: {{ $repair->center->name }}</h5>
          <p class="card-text">
            ชื่อ-สกุล: {{ $repair->citizen->fullname }}
          </p>
          <p class="card-text">
            สิ่งของที่นำซ่อม: {{ $repair->item->item_type->name }}
          </p>
          <p class="card-text">
            อาการเสีย: {{ $repair->item->problem }}
          </p>
          <p class="card-text">
            รายละเอียด: {{ $repair->item->description }}
          </p>
          <p class="card-text">
            ผลการซ่อม: {{ $repair->result_name }}
          </p>
          <p class="card-text">
            รายละเอียดการซ่อม: {{ $repair->description }}
          </p>
          @foreach ($repair->item->flies()->photo()->get() as $photo)
            <img src="{!! '/upload/items/' . $photo->destination !!}" width="200" height="200" />
          @endforeach
        </div>
      </div>

    </div>
    <div class="col"></div>

  </div>
@endsection
