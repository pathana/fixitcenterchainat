<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    {{-- <title>{{ config('app.name', 'Laravel') }}</title> --}}
    <title>Fixitcenter chaiant - @yield('title')</title>

    <!-- Styles -->
    <link href="{{ asset('css/landing.css') }}" rel="stylesheet">
</head>
<body>
  <!-- Navigation -->
  <nav class="navbar navbar-light bg-orange static-top">
    <div class="container">
      <a class="navbar-brand text-white" href="/">
        <img src="{!! '/images/null_img.png' !!}" width="30" height="30" class="d-inline-block align-top" />
        Fixit Center Chainat
      </a>
      {{-- Authentication Link --}}
      @guest
        <a class="btn btn-primary text-white" href="{{ route('login') }}">เข้าสู่ระบบ</a>
      @else
        <div class="dropdown">
          <button
            class="btn btn-dark dropdown-toggle text-white"
            type="button"
            id="dropdownMenuButton"
            data-toggle="dropdown"
            aria-haspopup="true"
            aria-expanded="false">
            รายการ
          </button>
          <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
            <a class="dropdown-item" href="{{ url('center/dashboard')}}">ลงทะเบียน</a>
            <a class="dropdown-item" href=" {{ route('ratings.index')}}">สถิติความพึงพอใจ</a>
            <a class="dropdown-item" href="{{ route('logout')}}">ออกจากระบบ</a>
          </div>
        </div>
      @endguest
      {{-- End of Authentication Link --}}
    </div>
  </nav>
  {{-- End of Navigation --}}
  @yield('content')
  <!-- Footer -->
  {{-- <footer class="footer bg-light fixed-bottom"> --}}
  @yield('footer-class')
    <div class="container">
      <div class="row">
        <div class="col-lg-12 h-100 text-center my-auto">
          <p class="text-muted small mb-4 mb-lg-0">&copy; fixitcenterchainat.com 2019. All Rights Reserved.</p>
        </div>
      </div>
    </div>
  </footer>

  <script src="{{ asset('js/app.js')}}"></script>
  {{-- End of Footer --}}
</body>
