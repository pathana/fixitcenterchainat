@extends('layouts.landing')

@section('title', 'หน้าแรก')

@section('content')

<!-- Icons Grid -->
<section class="features-icons bg-light text-center">
 <div class="container">
   <div class="row">
     <div class="col-lg-3 col-md-3 col-12 offset-lg-1 offset-md-1">
       <div class="features-icons-item mx-auto mb-5 mb-lg-0 mb-lg-3">
         <div class="features-icons-icon d-flex">
           <i class="fas fa-tasks m-auto text-primary"></i>
         </div>
         <h3>หน้าที่และความรับผิดชอบ</h3>
         <a href="{{ route('mission')}}" class="stretched-link">ดูเพิ่มเติม</a>
       </div>
     </div>
     <div class="col-lg-3 col-md-3 col-12">
       <div class="features-icons-item mx-auto mb-5 mb-lg-0 mb-lg-3">
         <div class="features-icons-icon d-flex">
           <i class="fas fa-sitemap m-auto text-primary"></i>
         </div>
         <h3>บุคลากรงานโครงการพิเศษ</h3>
         <a href="{{ route('personnel')}}" class="stretched-link">ดูเพิ่มเติม</a>
       </div>
     </div>
     <div class="col-lg-3 col-md-3 col-12">
       <div class="features-icons-item mx-auto mb-5 mb-lg-0 mb-lg-3">
         <div class="features-icons-icon d-flex">
           <i class="fas fa-star-half-alt m-auto text-primary"></i>
         </div>
         <h3>ความพึงพอใจ</h3>
         <a href="{{ route('ratings.create')}}" class="stretched-link">ดูเพิ่มเติม</a>
       </div>
     </div>
   </div>
 </div>
</section>

<!-- Image Showcases -->
<section class="showcase mt-3">
  <div class="container-fluid p-0">
    <div class="row">
      <div class="col"></div>
      <div class="col-lg-3">
        <div class="card">
          <div class="card-header bg-orange text-white">
            <h6 class="m-0 font-weight-bold">ข่าวประชาสัมพันธ์</h6>
          </div>
          <div class="card-body">
            <ul class="list-unstyled">
              @foreach ($advertises as $advertise)
                <li class="media my-4">
                  <img
                    class="mr-3 rounded border border-warning"
                    src="{!! !empty($advertise->flies()->photo()->first()) ? $advertise->file_path . $advertise->flies()->photo()->first()->destination : 'http://placehold.it/750x300' !!}"
                    width="100"
                    height="100"
                  />
                  <div class="media-body">
                    <h5 class="mt-0 mb-1">{{ str_limit($advertise->title, $limit = 50, $end = '...')}}</h5>
                    <p class="card-text">{{ $advertise->created_at }}</p>
                    <a href="/post/{{ $advertise->id }}" target="_blank">อ่านเพิ่มเติ่ม &rarr;</a>
                  </div>
                </li>
              @endforeach
            </ul>
            <div class="text-right">
              <a href="/allpost?type=advertise">อ่านทั้งหมด</a>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-3">
        <div class="card">
          <div class="card-header bg-orange text-white">
            <h6 class="m-0 font-weight-bold">บริการข้อมูล</h6>
          </div>
          <div class="card-body">
            <ul class="list-unstyled">
              @foreach ($informations as $info)
                <li class="media my-4">
                  <img
                    class="mr-3 rounded border border-warning"
                    src="{!! !empty($info->flies()->photo()->first()) ? $info->file_path . $info->flies()->photo()->first()->destination : 'http://placehold.it/750x300' !!}"
                    width="100"
                    height="100"
                  />
                  <div class="media-body">
                    <h5 class="mt-0 mb-1">{{ str_limit($info->title, $limit = 50, $end = '...')}}</h5>
                    <p class="card-text">{{ $info->created_at }}</p>
                    <a href="/post/{{ $info->id }}" target="_blank">อ่านเพิ่มเติ่ม &rarr;</a>
                  </div>
                </li>
              @endforeach
            </ul>
            <div class="text-right">
              <a href="/allpost?type=information">อ่านทั้งหมด</a>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-3">
        <div class="card">
          <div class="card-header bg-orange text-white">
            <h6 class="m-0 font-weight-bold">ภาพกิจกรรม</h6>
          </div>
          <div class="card-body">
            <ul class="list-unstyled">
              @foreach ($activities as $activity)
                <li class="media my-4">
                  <img
                    class="mr-3 rounded border border-warning"
                    src="{!! !empty($activity->flies()->photo()->first()) ? $activity->file_path . $activity->flies()->photo()->first()->destination : 'http://placehold.it/750x300' !!}"
                    width="100"
                    height="100"
                  />
                  <div class="media-body">
                    <h5 class="mt-0 mb-1">{{ str_limit($activity->title, $limit = 50, $end = '...')}}</h5>
                    <p class="card-text">{{ $activity->created_at }}</p>
                    <a href="/post/{{ $activity->id }}" target="_blank">อ่านเพิ่มเติ่ม &rarr;</a>
                  </div>
                </li>
              @endforeach
            </ul>
            <div class="text-right">
              <a href="/allpost?type=activities">อ่านทั้งหมด</a>
            </div>
          </div>
        </div>
      </div>
      <div class="col"></div>
    </div>
  </div>
</section>
@endsection

@section('footer-class')
  <footer class="footer bg-light">
@endsection
