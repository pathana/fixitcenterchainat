@extends('layouts.landing')

@section('title', 'ข่าวประชาสัมพันธ์ทั้งหมด')

@section('content')
  <div class="row mt-3 pb-3">
    <div class="col-lg-6 offset-lg-3">
      <div class="clearfix">
        <h5 class="float-left"> {{ $postHeader }} </h5>
        <form action="/allpost" method="POST" role="search" class="float-right form-inline">
          {{ csrf_field()}}
          <div class="input-group">
            <select class="custom-select" name="month" id="monthSelect">
              <option selected value="">เลือกเดือน</option>
              @for ($i = 1; $i <= 12; $i++)
                <option
                  value={{$i}}
                  @if ($oldMonth == $i)
                    selected
                  @endif
                >
                  {{ $i }}
                </option>
              @endfor
            </select>
          </div>
          <div class="input-group">
            <select class="custom-select" name="year" id="yearSelect">
              <option selected value="">เลือกปี</option>
              @for ($i = 0; $i <= 10; $i++)
                <option
                  value={{now()->year - $i}}
                  @if ($oldYear == now()->year - $i)
                    selected
                  @endif
                >
                  {{ now()->year - $i }}
                </option>
              @endfor
            </select>
          </div>
          <div class="input-group">
            <input type="text" class="form-control" name="search" placeholder="ค้นหาหัวข้อ" value="{{ $oldSearch }}" />
            <span class="input-group-btn">
              <button type="submit" class="btn btn-primary">
                <i class="fas fa-search"></i>
              </button>
            </span>
          </div>
          <input type="hidden" name="type" value="{{ $type }}"/>
        </form>
      </div>

      <ul class="list-unstyled">
        @foreach ($posts as $post)
          <li class="media my-4">
            <img
              class="mr-3 rounded border border-warning"
              src="{!! !empty($post->flies()->photo()->first()) ? $post->file_path . $post->flies()->photo()->first()->destination : 'http://placehold.it/750x300' !!}"
              width="100"
              height="100"
            />
            <div class="media-body">
              <h5 class="mt-0 mb-1">{{ str_limit($post->title, $limit = 50, $end = '...')}}</h5>
              <p class="card-text">{{ $post->created_at }}</p>
              <a href="/post/{{ $post->id }}" target="_blank">อ่านเพิ่มเติ่ม &rarr;</a>
            </div>
          </li>
        @endforeach
      </ul>

      <div class="text-center">
        {{ $posts->links() }}
      </div>

    </div>
  </div>
@endsection

@section('footer-class')
  <footer class="footer bg-light">
@endsection
