/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */



require('./bootstrap');

import Vue from 'vue'
import VueRouter from 'vue-router'
import tinymce from 'vue-tinymce-editor'
import LaravelVuePagination from 'laravel-vue-pagination'
import DatePicker from 'vue2-datepicker'
import VueFlashMessage from 'vue-flash-message'

import BootstrapVue from 'bootstrap-vue'
import BForm from 'bootstrap-vue/es/components/form/form'
import BFormGroup from 'bootstrap-vue/es/components/form-group/form-group'
import BFormInput from 'bootstrap-vue/es/components/form-input/form-input'

import Homepage from './components/admin/Homepage'
import LandingIndex from './components/admin/landing/Index'
import LandingUpdate from './components/admin/landing/Update'

import AdvertiseIndex from './components/admin/advertise/Index'
import AdvertiseCreate from './components/admin/advertise/Create'
import AdvertiseUpdate from './components/admin/advertise/Update'

import ActivitiesIndex from './components/admin/activities/Index'
import ActivitiesCreate from './components/admin/activities/Create'
import ActivitiesUpdate from './components/admin/activities/Update'

import InformationIndex from './components/admin/information/Index'
import InformationCreate from './components/admin/information/Create'
import InformationUpdate from './components/admin/information/Update'

Vue.use(VueRouter)
Vue.use(VueFlashMessage)

Vue.use(BootstrapVue)

Vue.component('b-form', BForm)
Vue.component('b-form-group', BFormGroup)
Vue.component('b-form-input', BFormInput)

Vue.component('tinymce', tinymce)
Vue.component('pagination', LaravelVuePagination)
Vue.component('date-picker', DatePicker)

const router = new VueRouter({
	mode: 'history',
	linkActiveClass: "active",
	routes: [
		{
			path: '/admin/dashboard',
			name: 'landingIndex',
			component: LandingIndex,
			props: true
		},
		{
			path: '/admin/landing/update/:richtextId',
			name: 'landingUpdate',
			component: LandingUpdate,
			props: true
		},
    // advertise route
		{
			path: '/admin/advertise',
			name: 'advertiseIndex',
			component: AdvertiseIndex,
			props: true
		},
		{
			path: '/admin/advertise/create',
			name: 'advertiseCreate',
			component: AdvertiseCreate,
			props: true
		},
		{
			path: '/admin/advertise/update/:postId',
			name: 'advertiseUpdate',
			component: AdvertiseUpdate,
			props: true
		},
    // activities route
		{
			path: '/admin/activities',
			name: 'activitiesIndex',
			component: ActivitiesIndex,
			props: true,
		},
		{
			path: '/admin/activities/create',
			name: 'activitiesCreate',
			component: ActivitiesCreate,
			props: true

		},
		{
			path: '/admin/activities/update/:postId',
			name: 'activitiesUpdate',
			component: ActivitiesUpdate,
      props: true
		},
    // information route
		{
			path: '/admin/information',
			name: 'informationIndex',
			component: InformationIndex,
			props: true,
		},
		{
			path: '/admin/information/create',
			name: 'informationCreate',
			component: InformationCreate,
			props: true

		},
		{
			path: '/admin/information/update/:postId',
			name: 'informationUpdate',
			component: InformationUpdate,
      props: true
		},
	]
});

const admin = new Vue({
	el: '#admin',
	router,
	components: {
		Homepage,
	},
});
