require('./bootstrap');

import Vue from 'vue'
import VueRouter from 'vue-router'
import LaravelVuePagination from 'laravel-vue-pagination'

import Homepage from './components/inventories/Homepage'

import InventoriesIndex from './components/inventories/Index'
import PartIndex from './components/inventories/part/Index'
import PartCreate from './components/inventories/part/Create'
import PartAdd from './components/inventories/part/Add'
import PartHistory from './components/inventories/part/History'
import PartEdit from './components/inventories/part/Edit'
import PartUpload from './components/inventories/part/Upload'

import BootstrapVue from 'bootstrap-vue'
import BForm from 'bootstrap-vue/es/components/form/form'
import BFormGroup from 'bootstrap-vue/es/components/form-group/form-group'
import BFormInput from 'bootstrap-vue/es/components/form-input/form-input'
import BAlert from 'bootstrap-vue/es/components/alert/alert'
import BFormFile from 'bootstrap-vue/es/components/form-file/form-file'
import BCollapse from 'bootstrap-vue/es/components/collapse/collapse'
import BButton from 'bootstrap-vue/es/components/button/button'
import BFormSelect from 'bootstrap-vue/es/components/form-select/form-select'

Vue.use(VueRouter)
Vue.use(BootstrapVue)

Vue.component('pagination', LaravelVuePagination)

Vue.component('b-form', BForm)
Vue.component('b-form-group', BFormGroup)
Vue.component('b-form-input', BFormInput)
Vue.component('b-alert', BAlert)
Vue.component('b-form-file', BFormFile)
Vue.component('b-form-select', BFormSelect)
Vue.component('b-collapse', BCollapse)
Vue.component('b-button', BButton)

const router = new VueRouter({
  mode: 'history',
  routes: [
    {
      path: '/inventories/dashboard',
      name: 'inventoriesIndex',
      component: InventoriesIndex,
      props: true
    },
    {
      path: '/inventories/:id/index',
      name: 'partIndex',
      component: PartIndex,
      props: true
    },
    {
      path: '/inventories/:id/create',
      name: 'partCreate',
      component: PartCreate,
      props: true
    },
    {
      path: '/inventories/:dept_id/part/:part_id',
      name: 'partAdd',
      component: PartAdd,
      props: true
    },
    {
      path: '/inventories/:dept_id/part/:part_id/history',
      name: 'partHistory',
      component: PartHistory,
      props: true
    },
    {
      path: '/inventories/:dept_id/part/:part_id/edit',
      name: 'partEdit',
      component: PartEdit,
      props: true
    },
    {
      path: '/inventories/:id/upload',
      name: 'partUpload',
      component: PartUpload,
      props: true
    },
  ]
})

const inventories = new Vue({
  el: '#inventories',
  router,
  components: {
    Homepage,
  }
})
