require('./bootstrap');

import Vue from 'vue'
import VueRouter from 'vue-router'
import LaravelVuePagination from 'laravel-vue-pagination'
import onlyInt from 'vue-input-only-number'
import VueMoment from 'vue-moment'

import BootstrapVue from 'bootstrap-vue'
import BForm from 'bootstrap-vue/es/components/form/form'
import BFormGroup from 'bootstrap-vue/es/components/form-group/form-group'
import BFormInput from 'bootstrap-vue/es/components/form-input/form-input'
import BAlert from 'bootstrap-vue/es/components/alert/alert'
import BFormFile from 'bootstrap-vue/es/components/form-file/form-file'
import BCollapse from 'bootstrap-vue/es/components/collapse/collapse'
import BButton from 'bootstrap-vue/es/components/button/button'
import BListGroup from 'bootstrap-vue/es/components/list-group/list-group'
import BTable from 'bootstrap-vue'

import Homepage from './components/center/Homepage'

import CenterIndex from './components/center/Index'
import CenterCreate from './components/center/Create'
import CenterUpdate from './components/center/Update'

import CitizenIndex from './components/center/citizen/Index'
import CitizenCreate from './components/center/citizen/Create'
import CitizenUpdate from './components/center/citizen/Update'
import CitizenShow from './components/center/citizen/Show'
import CitizenRepair from './components/center/citizen/Repair'
import CitizenCenter from './components/center/citizen/Center'

import BuildIndex from './components/center/build/Index'
import BuildCreate from './components/center/build/Create'
import BuildUpdate from './components/center/build/Update'
import BuildCitizen from './components/center/build/Citizen'
import BuildOfficer from './components/center/build/Officer'

import RepairWaitRepair from './components/center/repair/WaitRepair'
import RepairWaitSendback from './components/center/repair/WaitSendback'
import RepairFinished from './components/center/repair/Finished'
import RepairCreate from './components/center/repair/Create'
import RepairSearch from './components/center/Search'

import ItemShow from './components/center/item/Show'
import ItemEdit from './components/center/item/Edit'
import ItemFix from './components/center/item/Fix'
import ItemReturn from './components/center/item/Return'

import ReportIndex from './components/center/report/Index'

const moment = require('moment')
require('moment/locale/th')

Vue.use(VueRouter)
Vue.use(BootstrapVue)
Vue.use(onlyInt)
Vue.use(VueMoment, {
  moment
})

Vue.component('b-form', BForm)
Vue.component('b-form-group', BFormGroup)
Vue.component('b-form-input', BFormInput)
Vue.component('b-alert', BAlert)
Vue.component('b-form-file', BFormFile)
Vue.component('b-collapse', BCollapse)
Vue.component('b-button', BButton)
Vue.component('b-list-group', BListGroup)
Vue.component('b-table', BTable)

Vue.component('pagination', LaravelVuePagination)

const router = new VueRouter({
  mode: 'history',
  routes: [
    {
      path: '/center/dashboard',
      name: 'centerIndex',
      component: CenterIndex,
      props: true
    },
    {
      path: '/center/create',
      name: 'centerCreate',
      component: CenterCreate,
      props: true
    },
    {
      path: '/center/:id/update',
      name: 'centerUpdate',
      component: CenterUpdate,
      props: true
    },
    {
      path: '/center/search',
      name: 'repairSearch',
      component: RepairSearch,
      props: true
    },

    {
      path: '/center/citizen',
      name: 'citizenIndex',
      component: CitizenIndex,
      props: true
    },
    {
      path: '/center/citizen/create',
      name: 'citizenCreate',
      component: CitizenCreate,
      props: true
    },
    {
      path: '/center/citizen/update/:id',
      name: 'citizenUpdate',
      component: CitizenUpdate,
      props: true
    },
    {
      path: '/center/citizen/show/:id',
      name: 'citizenShow',
      component: CitizenShow,
      props: true
    },
    {
      path: '/center/citizen/:id/repairs',
      name: 'citizenRepair',
      component: CitizenRepair,
      props: true
    },
    {
      path: '/center/citizen/:id/centers',
      name: 'citizenCenter',
      component: CitizenCenter,
      props: true
    },

    {
      path: '/center/build',
      name: 'buildIndex',
      component: BuildIndex,
      props: true
    },
    {
      path: '/center/build/create',
      name: 'buildCreate',
      component: BuildCreate,
      props: true
    },
    {
      path: '/center/build/:id/update',
      name: 'buildUpdate',
      component: BuildUpdate,
      props: true
    },
    {
      path: '/center/build/:id/citizen',
      name: 'buildCitizen',
      component: BuildCitizen,
      props: true
    },
    {
      path: '/center/build/:id/officer',
      name: 'buildOfficer',
      component: BuildOfficer,
      props: true
    },

    {
      path: '/center/:id/repair/wait_repair',
      name: 'repairWaitRepair',
      component: RepairWaitRepair,
      props: true
    },
    {
      path: '/center/:id/repair/wait_sendback',
      name: 'repairWaitSendback',
      component: RepairWaitSendback,
      props: true
    },
    {
      path: '/center/:id/repair/finished',
      name: 'repairFinished',
      component: RepairFinished,
      props: true
    },
    {
      path: '/center/:id/repair/create',
      name: 'repairCreate',
      component: RepairCreate,
      props: true
    },

    {
      path: '/center/item/:id/show',
      name: 'itemShow',
      component: ItemShow,
      props: true
    },
    {
      path: '/center/item/:id/edit',
      name: 'itemEdit',
      component: ItemEdit,
      props: true
    },
    {
      path: '/center/item/:id/fix',
      name: 'itemFix',
      component: ItemFix,
      props: true
    },
    {
      path: '/center/item/:id/return',
      name: 'itemReturn',
      component: ItemReturn,
      props: true
    },
    {
      path: '/center/:id/report',
      name: 'reportIndex',
      component: ReportIndex,
      props: true
    },


  ]
})

const center = new Vue({
  el: '#center',
  router,
  components: {
    Homepage,
  }
})
