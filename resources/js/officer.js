require('./bootstrap');

import Vue from 'vue'
import VueRouter from 'vue-router'
import LaravelVuePagination from 'laravel-vue-pagination'

import Homepage from './components/officer/Homepage'

import OfficerIndex from './components/officer/Index'
import OfficerCreate from './components/officer/Create'
import OfficerEdit from './components/officer/Edit'

import BootstrapVue from 'bootstrap-vue'
import BForm from 'bootstrap-vue/es/components/form/form'
import BFormGroup from 'bootstrap-vue/es/components/form-group/form-group'
import BFormInput from 'bootstrap-vue/es/components/form-input/form-input'
import BAlert from 'bootstrap-vue/es/components/alert/alert'
import BFormFile from 'bootstrap-vue/es/components/form-file/form-file'
import BCollapse from 'bootstrap-vue/es/components/collapse/collapse'
import BButton from 'bootstrap-vue/es/components/button/button'
import BFormSelect from 'bootstrap-vue/es/components/form-select/form-select'

Vue.use(VueRouter)
Vue.use(BootstrapVue)

Vue.component('pagination', LaravelVuePagination)

Vue.component('b-form', BForm)
Vue.component('b-form-group', BFormGroup)
Vue.component('b-form-input', BFormInput)
Vue.component('b-alert', BAlert)
Vue.component('b-form-file', BFormFile)
Vue.component('b-form-select', BFormSelect)
Vue.component('b-collapse', BCollapse)
Vue.component('b-button', BButton)

const router = new VueRouter({
  mode: 'history',
  routes: [
    {
      path: '/officer/dashboard',
      name: 'officerIndex',
      component: OfficerIndex,
      props: true
    },
    {
      path: '/officer/create',
      name: 'officerCreate',
      component: OfficerCreate,
      props: true
    },
    {
      path: '/officer/:id/edit',
      name: 'officerEdit',
      component: OfficerEdit,
      props: true
    },
  ]
})

const officer = new Vue({
  el: '#officer',
  router,
  components: {
    Homepage,
  }
})
