const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
   .sass('resources/sass/app.scss', 'public/css');

mix.sass('resources/sass/landing.scss', 'public/css');


mix.webpackConfig({
  resolve: {
    extensions: ['.vue'],
    alias: {
      '@Vue': __dirname + '/resources/js/components'
    }
  },
  module: {
    rules: [
      {
        test: /\.pug$/,
        loader: 'pug-plain-loader'
      }
    ]
  }
});

mix.js('resources/js/admin.js', 'public/js')
mix.js('resources/js/officer.js', 'public/js')
mix.js('resources/js/inventories.js', 'public/js')
mix.sass('resources/sass/admindashboard.scss', 'public/css')
mix.js('resources/js/center.js', 'public/js')
