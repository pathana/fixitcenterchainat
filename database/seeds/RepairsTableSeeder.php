<?php

use App\Repair;
use App\Center;
use App\Citizen;
use App\Item;
use App\Performer;
use App\Officer;
use Illuminate\Database\Seeder;
use Carbon\Carbon;

class RepairsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $center = Center::find(1);
      $citizen = Citizen::find(1);
      $citizen_total = Citizen::count();

      $status = [
        'wait',
        'reparable',
        'irreparable',
        'forward'
      ];

      $finished = [
        true, false
      ];

      $officer_total = Officer::count();

      for($i = 1; $i <= 20; $i++) {
        // $performer = Performer::create([
        //   'register' => rand(1, $officer_total),
        //   'register_date' => Carbon::now()
        // ]);

        $performer = new Performer;
        $performer->register = rand(1, $officer_total);
        $performer->register_date = Carbon::now();

        $item = Item::find($i);

        $repair = new Repair;
        $repair->center_id = $center->id;
        $repair->citizen_id = rand(1, $citizen_total);
        // $repair->item_id = $i;

        // $repair->performer_id = $performer->id;
        $repair->result = $status[rand(0, 3)];
        $repair->finished = $finished[rand(0, 1)];
        $repair->start_date = Carbon::now();
        $repair->save();
        $repair->item()->save($item);
        $repair->performer()->save($performer);
      }
    }
}
