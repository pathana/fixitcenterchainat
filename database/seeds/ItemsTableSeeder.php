<?php

use App\Item;
use App\Fly;
use App\ItemSuggestion;
use Faker\Factory;
use Illuminate\Database\Seeder;
use Faker\Provider\Lorem;

class ItemsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $faker = Factory::create();
      $targetpath = public_path('upload/items/');
      $photopath = database_path('photos/');

      $types = ['vehicle', 'electronics', 'machine', 'other'];
      $suggestions = ItemSuggestion::all()->map(function ($item, $key) {
        return $item->name;
      });
      $suggestions = $suggestions->all();
      $item_total = ItemSuggestion::count();

      for($i = 0; $i < 20; $i++) {
        $item = new Item;
        $item->name = $suggestions[rand(0, $item_total-1)];
        $item->type = $types[rand(0,3)];
        $item->problem = $suggestions[rand(0, $item_total-1)];
        $item->status = $suggestions[rand(0, $item_total-1)];
        $item->attach = $suggestions[rand(0, $item_total-1)];
        $item->description = $suggestions[rand(0, $item_total-1)];
        $item->repair_id = 0;
        $item->item_type_id = 0;
        $item->save();

        $lists = collect([]);

        for ($j = 0; $j < 3; $j++) {
          $photo = new Fly;
          $photo->name = $faker->sentence;
          $photo->destination = $faker->file($photopath, $targetpath, false);
          $photo->extension = 'photo';
          $lists->push($photo);
        }

        $item->flies()->saveMany($lists);
      }

    }
}
