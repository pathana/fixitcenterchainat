<?php

use App\Officer;
use Faker\Factory;
use Faker\Provider\Person;
use Faker\Provider\th_TH\phoneNumber;
use Faker\Provider\Barcode;
use Illuminate\Database\Seeder;

class OfficersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $at = [
        'ชำนาญ แก้วจงประสิทธ์ิ',
        'กวีศักดิ์ แป้นขาว',
        'ประสิทธ์ิ ถือแก้ว',
        'ศราวุธ แพ่งประสิทธิ์',
        'ณฤทธิ์ อุ่มสุข',
        'ปรัชญา โพธี',
        'ธนากรณ์ จินดารัตน์',
        'วิทยา อยู่ภาโส',
        'สุเทพ อินอุดม',
        'เสริมศักดิ์ สุวรรณาลัย',
        'ไพโรจน์ บัณฑิตย์',
        'ปริญญา ศรีวิชาสร้อย',
        'ธวัชชัย พาดฤทธิ์',
        'ทวีศักดิ์ เทียนชัย',
        'สนธยา เสือน้อย',
        'กิติศักดิ์ โพธิ์กระจ่าง',
        'นเรนทร์ สมสมัย',
        'สัมฤทธิ์ แดงกรัด',
        'ศรัณย์ภัทร บุญญา',
        'เอนกนัฐ เรือนไทย',
        'อลงกต ดำนิล',
      ];

      $el = [
        'ธนูศักดิ์ อรุณไพร',
        'ภาวิณี ปานันตา',
        'สมศักดิ์ เหมาะสมัย',
        'สุทัศน์ ศุภนคร',
        'จิรพงษ์ ทองคำรัตน์',
        'วัชระ นิเกศรี',
        'ศักดิ์ดา ฉ่ำช้างทอง',
        'อนุกูล เชื้อน้อย',
        'วิไลวรรณ์ จันทร์จิตร',
      ];

      $ep = [
        "วิทยา ชูชื่น",
        "อนุสรณ์ วงศ์ปินจันทร์",
        "ยุทธกร ไชยสิงห์",
        "พีรภัทร ฉัตรชัยวัฒนา",
        "สุภาษณ์ การภักดี",
        "สุวรรณี งามนิล",
        "ธรรมนูญ บุญปราการ",
        "ปรารถนา บุ้งทิม",
        "ชนันท์วัลย์ ศุภภูดิสโสภณ",
        "สมโภชน์ ตามสายลม",
        "ธน สังข์ทอง",
        "ธัญวัฒน์ ทองสุก",
      ];

      $faker = Factory::create('th_TH');
      foreach ($at as $key => $value) {
        $officer = Officer::create([
          'fullname' => $value,
          'citizen_number' => $faker->ean13,
          'phonenumber' => $faker->phoneNumber,
          'department_id' => 3,
          'type' => 'teacher'
        ]);
      }

      foreach ($el as $key => $value) {
        $officer = Officer::create([
          'fullname' => $value,
          'citizen_number' => $faker->ean13,
          'phonenumber' => $faker->phoneNumber,
          'department_id' => 2,
          'type' => 'teacher'
        ]);
      }

      foreach ($ep as $key => $value) {
        $officer = Officer::create([
          'fullname' => $value,
          'citizen_number' => $faker->ean13,
          'phonenumber' => $faker->phoneNumber,
          'department_id' => 1,
          'type' => 'teacher'
        ]);
      }

      for ($i = 1; $i <= 4; $i++) {
        for ($j = 0; $j < 5; $j++) {
          $officer = Officer::create([
            'fullname' => $faker->name,
            'citizen_number' => $faker->ean13,
            'phonenumber' => $faker->phoneNumber,
            'department_id' => $i,
            'type' => 'student'
          ]);
        }
      }

      $officer = Officer::create([
        'fullname' => 'ปทุมวรรณ  จิ๋วสกุล',
        'citizen_number' => $faker->ean13,
        'phonenumber' => $faker->phoneNumber,
        'department_id' => 4,
        'type' => 'teacher'
      ]);
    }
}
