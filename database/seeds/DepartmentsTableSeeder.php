<?php

use App\Department;
use Illuminate\Database\Seeder;

class DepartmentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $departmentName = ['ช่างไฟฟ้ากำลัง', 'ช่างอิเล็กทรอนิกส์', 'ช่างยนต์', 'เจ้าหน้าที่'];
      $departmentAKA = ['ep', 'el', 'at', 'of'];

      for ($i = 0; $i < count($departmentAKA); $i++) {
        $department = Department::create([
          'name' => $departmentName[$i],
          'aka' => $departmentAKA[$i],
        ]);
      }
    }
}
