<?php

use App\Role;
use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $roles = array(
        "admin" => "ผู้ดูแลระบบ",
        "officer" => "เจ้าหน้าที่",
        "ep_student" => "นักศึกษาช่างไฟฟ้า",
        "el_student" => "นักศึกษาช่างอิเล็กทรอนิกส์",
        "at_student" => "นักศึกษาช่างยนต์",
        "ep_teacher" => "ครูช่างไฟฟ้า",
        "el_teacher" => "ครูช่างอิเล็กทรอนิกส์",
        "at_teacher" => "ครูช่างยนต์",
      );

      foreach ($roles as $key => $value) {
        $role_regular_user = new Role;
        $role_regular_user->name = $key;
        $role_regular_user->description = $value;
        $role_regular_user->save();
      }
    }
}
