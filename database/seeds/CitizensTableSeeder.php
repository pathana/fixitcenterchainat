<?php

use App\Citizen;
use Faker\Factory;
use Faker\Provider\File;
use Faker\Provider\th_TH\Person;
use Faker\Provider\th_TH\Address;
use Faker\Provider\th_TH\phoneNumber;
use Faker\Provider\Barcode;
use Illuminate\Database\Seeder;

class CitizensTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $citizenFile = database_path() . '/customer.json';
      $photoPath = database_path('piccustomer/');

      $strJsonFile = file_get_contents($citizenFile);
      $data = json_decode($strJsonFile);
      $customers = $data[2]->data;

      for ($i = 0; $i < count($customers); $i++) {
        $customer = $customers[$i];
        $people = new Citizen;
        $people->title = $customer->Cus_Sex;
        $people->fullname = $customer->Cus_Name . '  ' . $customer->Cus_Surename;
        $people->number = $customer->Cus_Id;
        $people->phonenumber = $customer->Cus_Tel;
        $address = $customer->Number .
          ' หมู่ ' . $customer->section .
          ' ถนน ' . $customer->Road .
          ' ตำบล ' . $customer->district .
          ' อำเภอ ' . $customer->canton .
          ' จังหวัด ' . $customer->province . ' ' . $customer->zipcode;
        $people->address = $address;
        $people->avatar = $customer->pic;
        // try {
        //   // $image = imagecreatefromjpeg($photoPath . $customer->pic);
        //   $image = @imagecreatefromjpeg($photoPath . $customer->pic);
        //   $destination = public_path('upload/citizens/') . $customer->pic;
        //   imagejpeg($image, $destination, 50);
        //   imagedestroy($image);
        // } catch (Exception $e) {
        //   var_dump($e->getMessage());
        //   var_dump($customer->pic);
        // }
        $people->save();
      }
    }
}
