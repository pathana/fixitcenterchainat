<?php

use App\SparePart;
use App\Inventory;
use App\User;
use Illuminate\Database\Seeder;

class SparePartsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $files = [
        'ep' => [
          'file' => 'ep-part.csv',
          'id' => 1
        ],
        'el' => [
          'file' => 'el-part.csv',
          'id' => 2
        ],
        'at' => [
          'file' => 'at-part.csv',
          'id' => 3
        ],
      ];

      $user = User::find(1);

      foreach ($files as $file) {
        $csv = array_map('str_getcsv', file(database_path() . '/' . $file['file']));

        for ($i = 1; $i < count($csv); $i++) {
          $spare = new SparePart;
          $spare->name = $csv[$i][0];
          $spare->department_id = $file['id'];
          $spare->category = $csv[$i][3];
          $spare->price = $csv[$i][2];
          $spare->unit = $csv[$i][5];
          $spare->save();

          $inventory = new Inventory;
          $inventory->spare_part_id = $spare->id;
          $inventory->cost = $csv[$i][1];
          $inventory->balance = $csv[$i][1];

          $user->inventories()->save($inventory);
        }
      }
    }
}
