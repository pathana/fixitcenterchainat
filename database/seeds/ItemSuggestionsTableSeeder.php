<?php

use App\ItemSuggestion;
use Illuminate\Database\Seeder;

class ItemSuggestionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $csv = array_map('str_getcsv', file(database_path() . '/item_names.csv'));
      for ($i = 0; $i < count($csv); $i++) {
        try {
          ItemSuggestion::create([
            'name' => $csv[$i][0]
          ]);
        }
        catch (exception $e) {
          continue;
        }
      }
    }
}
