<?php

use App\Rating;
use Illuminate\Database\Seeder;

class RatingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $peoples = 385;

      $sex_array = array();
      for($i = 0; $i < round($peoples * 0.406); $i++) array_push($sex_array, 1);
      for($i = 0; $i < round($peoples * 0.594); $i++) array_push($sex_array, 2);
      shuffle($sex_array);

      $age_array = array();
      for($i = 0; $i < round($peoples * 0.02); $i++) array_push($age_array, 1);
      for($i = 0; $i < round($peoples * 0.1); $i++) array_push($age_array, 2);
      for($i = 0; $i < round($peoples * 0.1); $i++) array_push($age_array, 3);
      for($i = 0; $i < round($peoples * 0.2); $i++) array_push($age_array, 4);
      for($i = 0; $i < round($peoples * 0.58); $i++) array_push($age_array, 5);
      shuffle($age_array);

      $career_array = array();
      for($i = 0; $i < round($peoples * 0.2); $i++) array_push($career_array, 1);
      for($i = 0; $i < round($peoples * 0.1); $i++) array_push($career_array, 2);
      for($i = 0; $i < round($peoples * 0.05); $i++) array_push($career_array, 3);
      for($i = 0; $i < round($peoples * 0.3); $i++) array_push($career_array, 4);
      for($i = 0; $i < round($peoples * 0.2); $i++) array_push($career_array, 5);
      for($i = 0; $i < round($peoples * 0.1); $i++) array_push($career_array, 6);
      for($i = 0; $i < round($peoples * 0.05); $i++) array_push($career_array, 7);
      shuffle($career_array);

      $education_array = array();
      for($i = 0; $i < round($peoples * 0.05); $i++) array_push($education_array, 1);
      for($i = 0; $i < round($peoples * 0.1); $i++) array_push($education_array, 2);
      for($i = 0; $i < round($peoples * 0.2); $i++) array_push($education_array, 3);
      for($i = 0; $i < round($peoples * 0.1); $i++) array_push($education_array, 4);
      for($i = 0; $i < round($peoples * 0.5); $i++) array_push($education_array, 5);
      for($i = 0; $i < round($peoples * 0.05); $i++) array_push($education_array, 6);
      shuffle($education_array);

      for($i = 0; $i < count($sex_array); $i++) {
        $options = array();
        for($j = 0; $j < 7; $j++) array_push($options, rand(4,5));
        $rating = Rating::create([
          'sex' => $sex_array[$i],
          'age' => $age_array[$i],
          'career' => $career_array[$i],
          'education' => $education_array[$i],
          'option1' => $options[0],
          'option2' => $options[1],
          'option3' => $options[2],
          'option4' => $options[3],
          'option5' => $options[4],
          'option6' => $options[5],
          'option7' => $options[6],
          'sum_option' => array_sum($options)
        ]);
      }

    }
}
