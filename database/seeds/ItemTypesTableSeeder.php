<?php

use App\ItemType;
use Illuminate\Database\Seeder;

class ItemTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $csv = array_map('str_getcsv', file(database_path() . '/item_options.csv'));
      for ($i = 1; $i < count($csv); $i++) {
        ItemType::create([
          'code' => $csv[$i][0],
          'name' => $csv[$i][1]
        ]);
      }
        //
    }
}
