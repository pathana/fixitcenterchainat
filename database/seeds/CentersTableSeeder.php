<?php

use App\Center;
use Faker\Factory;
use Illuminate\Database\Seeder;

class CentersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $center = Center::create([
        'name' => 'ศูนย์ถาวร',
        'amphoe' => 'เมืองชัยนาท',
        'province' => 'ชัยนาท'
      ]);

      $faker = Factory::create();

      for ($i = 0; $i < 40; $i++) {
        $center = Center::create([
          'name' => $faker->sentence,
          'amphoe' => $faker->sentence,
          'province' => $faker->sentence,
        ]);
      }
    }
}
