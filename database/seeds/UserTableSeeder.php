<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Role;
use Faker\Factory;
use Faker\Provider\Image;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $faker = Factory::create();

      $users = [
        'admin' => [
          'name' => 'admin',
          'email' => 'admin@fixit.com',
          'password' => bcrypt('adminfixit'),
        ],
        'officer' => [
          'name' => 'officer',
          'email' => 'officer@fixit.com',
          'password' => bcrypt('officerfixit'),
        ],
        'ep_student' => [
          'name' => 'ep_student',
          'email' => 'ep_student@fixit.com',
          'password' => bcrypt('ep_studentfixit'),
        ],
        'el_student' => [
          'name' => 'el_student',
          'email' => 'el_student@fixit.com',
          'password' => bcrypt('el_studentfixit'),
        ],
        'at_student' => [
          'name' => 'at_student',
          'email' => 'at_student@fixit.com',
          'password' => bcrypt('at_studentfixit'),
        ],
        'ep_teacher' => [
          'name' => 'ep_teacher',
          'email' => 'ep_teacher@fixit.com',
          'password' => bcrypt('ep_teacherfixit'),
        ],
        'el_teacher' => [
          'name' => 'el_teacher',
          'email' => 'el_teacher@fixit.com',
          'password' => bcrypt('el_teacherfixit'),
        ],
        'at_teacher' => [
          'name' => 'at_teacher',
          'email' => 'at_teacher@fixit.com',
          'password' => bcrypt('at_teacherfixit'),
        ],
      ];

      foreach ($users as $user) {
        $u = User::create([
          'name' => $user['name'],
          'email' => $user['email'],
          'password' => $user['password'],
          'avatar' => 'http://www.fixitcenter.org/fixit62/imgschool/null_img.png',
        ]);

        $u->roles()->attach(Role::where('name', $user['name'])->first());
      }
    }
}
