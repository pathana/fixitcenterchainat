<?php

use Illuminate\Database\Seeder;

class ProductionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $this->call([
        RoleTableSeeder::class,
        UserTableSeeder::class,
        CitizensTableSeeder::class,
        DepartmentsTableSeeder::class,
        SparePartsTableSeeder::class,
        ItemSuggestionsTableSeeder::class,
        ItemTypesTableSeeder::class,
      ]);
    }
}
