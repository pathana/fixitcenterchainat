<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
      // Truncate all tables
      $tables = DB::select('SHOW TABLES');
      foreach ($tables as $table) {
        if ($table->Tables_in_fixit !== 'migrations')
          DB::table($table->Tables_in_fixit)->truncate();
      }

      $this->call([
        RoleTableSeeder::class,
        UserTableSeeder::class,
        PostTableSeeder::class,
        RichtextTableSeeder::class,
        CitizensTableSeeder::class,
        CentersTableSeeder::class,
        DepartmentsTableSeeder::class,
        OfficersTableSeeder::class,
        SparePartsTableSeeder::class,
        ItemSuggestionsTableSeeder::class,
        ItemsTableSeeder::class,
        RepairsTableSeeder::class,
        ItemTypesTableSeeder::class,
      ]);
    }
}
