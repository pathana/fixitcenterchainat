<?php

use App\Post;
use App\Photo;
use App\Document;
use App\Fly;
use Illuminate\Database\Seeder;
use Faker\Factory;
use Faker\Provider\File;
use Faker\Provider\Lorem;

class PostTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $faker = Factory::create();

      $targetpath = public_path('upload/posts/');
      $photopath = database_path('photos/');
      $filepath = database_path('documents/');
      $postType = ['advertise', 'activities', 'information'];

      foreach ($postType as $type) {
        for ($j = 0; $j < 20; $j++) {
          $post = new Post;
          $post->title = $faker->sentence($nbWords = 6, $variableNbWords = true);
          $post->description = $faker->text($maxNbChars = 200) ;
          $post->type = $type;
          $post->save();
          // $lists = collect([]);
          //
          // for ($i = 0; $i <= 5; $i++) {
          //   $photo = new Fly;
          //   $photo->name = $faker->sentence($nbWords = 6, $variableNbWords = true);
          //   $photo->destination = $faker->file($photopath, $targetpath, false);
          //   $photo->extension = 'photo';
          //   $lists->push($photo);
          //
          //   $file = new Fly;
          //   $file->name = $faker->sentence($nbWords = 6, $variableNbWords = true);
          //   $file->destination = $faker->file($filepath, $targetpath, false);
          //   $file->extension = 'file';
          //   $lists->push($file);
          // }
          //
          // $post->flies()->saveMany($lists);
        }
      }
    }
}
