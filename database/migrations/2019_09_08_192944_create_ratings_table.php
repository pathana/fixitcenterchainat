<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRatingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ratings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('sex');
            $table->integer('age');
            $table->integer('career');
            $table->integer('education');
            $table->integer('option1');
            $table->integer('option2');
            $table->integer('option3');
            $table->integer('option4');
            $table->integer('option5');
            $table->integer('option6');
            $table->integer('option7');
            $table->integer('sum_option');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ratings');
    }
}
