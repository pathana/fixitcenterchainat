<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePerformersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('performers', function (Blueprint $table) {
            $table->increments('id');
            // ผู้รับลงทะเบียน
            $table->integer('register')->unsigned()->nullable(); //
            $table->datetime('register_date')->nullable();
            // ผู้ประเมินงานซ่อม
            $table->integer('assessor')->unsigned()->nullable();
            $table->datetime('assessor_date')->nullable();
            // หัวหน้าชุดซ่อม
            $table->integer('chief')->unsigned()->nullable();
            $table->datetime('chief_date')->nullable();
            // ผู้ส่งซ่อม
            $table->integer('sender')->unsigned()->nullable();
            $table->datetime('sender_date')->nullable();
            // ผู้ดำเนินการซ่อม
            $table->integer('technician')->unsigned()->nullable();
            $table->integer('repair_id')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('performers');
    }
}
