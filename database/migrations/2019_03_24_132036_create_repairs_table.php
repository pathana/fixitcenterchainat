<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRepairsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('repairs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('center_id')->unsigned();
            $table->integer('citizen_id')->unsigned();
            $table->double('cost')->default(0);
            $table->text('description')->nullable();
            $table->datetime('start_date')->nullable();
            $table->datetime('end_date')->nullable();
            $table->enum('result', ['wait', 'reparable', 'irreparable', 'forward'])->default('wait');
            $table->string('forward')->nullable();
            $table->string('receiver_name')->nullable();
            $table->datetime('receiver_date')->nullable();
            $table->string('sender_name')->nullable();
            $table->datetime('sender_date')->nullable();
            $table->enum('citizen_review', [5,4,3,2,1])->nullable();
            $table->string('citizen_comment')->nullable();
            $table->enum('technician_review', [5,4,3,2,1])->nullable();
            $table->string('technician_comment')->nullable();
            $table->boolean('finished')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('repairs');
    }
}
