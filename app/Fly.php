<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use File;
use Illuminate\Support\Facades\Log;

class Fly extends Model
{
  protected $fillable = ['name', 'destination', 'extension'];

  public function fileable()
  {
    return $this->morphTo();
  }

  public function scopePhoto($query)
  {
    return $query->where('extension', 'photo');
  }

  public function scopeFile($query)
  {
    return $query->where('extension', 'file');
  }

  public static function compressImage($file, $destination)
  {
    $info = getimagesize($file);
    $image = '';

    if ($info['mime'] == 'image/jpeg')
      $image = imagecreatefromjpeg($file);

    if ($info['mime'] == 'image/gif')
      $image = imagecreatefromgif($file);

    if ($info['mime'] == 'image/png')
      $image = imagecreatefrompng($file);

    if (empty($image)) return;

    imagejpeg($image, $destination, 50);
  }

  public static function isPhoto($keyword)
  {
    $photo_extension = collect(['jpeg', 'jpg', 'png']);

    return (bool) $photo_extension->search($keyword);
  }

  public static function isFile($keyword)
  {
    $file_extension = collect(['docx', 'xlsx', 'pdf', 'txt', 'csv', 'doc', 'xls']);

    return (bool) $file_extension->search($keyword);
  }

  public function deletePath(string $path)
  {
    File::delete($path . $this->destination);

    $this->delete();
  }

  public function delete()
  {
    $path = $this->fileable->public_path;
    File::delete($path . $this->destination);

    return parent::delete();
  }
}
