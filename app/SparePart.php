<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SparePart extends Model
{
  protected $fillable = ['name', 'department', 'category', 'photo', 'price', 'unit'];

  public function inventories()
  {
    return $this->hasMany('App\Inventory');
  }

  public function department()
  {
    return $this->belongsTo(Department::class);
  }

  public function getBalanceAttribute()
  {
    $inventory = $this->inventories->last();

    if (empty($inventory)) return 0;
    return $inventory->balance;
  }
}
