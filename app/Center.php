<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Center extends Model
{
  protected $fillable = ['name', 'amphoe', 'province'];

  public function repairs()
  {
    return $this->hasMany(Repair::class);
  }
}
