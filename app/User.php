<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function roles()
    {
      return $this->belongsToMany(Role::class);
    }

    public function checkRoles($roles)
    {
      if (! is_array($roles)) $roles = [$roles];
      if (! $this->hasAnyRole($roles)) {
        auth()->logut();
        abort(404);
      }
    }

    public function getRoleNameAttribute()
    {
      $roles = $this->roles->first();
      $name = $roles->name;
      if (strrpos("_admin", $name) != false) return "admin";
      if (strrpos("_officer", $name) != false) return "admin";
      if (strrpos($name, "teacher") != false) return "teacher";
      if (strrpos($name, "student") != false) return "student";
      return "student";
    }

    public function hasAnyRole($roles): bool
    {
      return (bool) $this->roles()->whereIn('name', $roles)->first();
    }

    public function hasRole($role): bool
    {
      return (bool) $this->roles()->where('name', $role)->first();
    }

    public function getJWTIdentifier()
    {
      return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
      return [];
    }

    public function inventories()
    {
      return $this->morphMany(Inventory::class, 'inventoriable');
    }
}
