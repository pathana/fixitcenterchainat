<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Topup extends Model
{
  protected $fillable = [
    'name', 'teacher',
    'description', 'status',
    'start_date', 'end_date', 'hours'
  ];

  public function peoples()
  {
    return $this->morphMany('App\People', 'peopleable');
  }
}
