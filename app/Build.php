<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Build extends Model
{
  protected $fillable = [
    'name', 'teacher',
    'description', 'status',
    'start_date', 'end_date', 'hours'
  ];

  public function peoples()
  {
    return $this->morphMany('App\People', 'peopleable');
  }

  public function getStatusNameAttribute()
  {
    $name = [
      'wait' => 'รออบรม',
      'now' => 'กำลังอบรม',
      'finished' => 'อบรมเสร็จ'
    ];

    return $name[$this->status];
  }
}
