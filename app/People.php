<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Http\Resources\CitizenResource;
use App\Http\Resources\OfficerResource;

class People extends Model
{
  protected $fillable = ['type', 'citizen_id', 'officer_id'];

  public function peopleable()
  {
    return $this->morphTo();
  }

  public function citizen()
  {
    return $this->belongsTo(Citizen::class);
  }

  public function officer()
  {
    return $this->belongsTo(Officer::class);
  }

  public function getDetailAttribute()
  {
    $detail = [
      'citizen' => new CitizenResource($this->citizen),
      'officer' => new OfficerResource($this->officer)
    ];

    return $detail[$this->type];
  }

  public function scopeOfficer($query)
  {
    return $query->where('type', 'officer');
  }

  public function scopeCitizen($query)
  {
    return $query->where('type', 'citizen');
  }
}
