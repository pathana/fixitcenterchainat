<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Inventory extends Model
{

  protected $fillable = ['spare_part_id', 'cost', 'balance', 'description'];

  public function inventoriable()
  {
    return $this->morphTo();
  }

  public function getPerformerAttribute()
  {
    $performer = $this->inventoriable;
    $klass = class_basename($performer);
    return $klass . " | id : " . $performer->id;
  }

  public function spare_part()
  {
    return $this->belongsTo(SparePart::class);
  }
}
