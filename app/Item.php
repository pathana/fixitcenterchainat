<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
  protected $fillable = [
    // 'name',
    // 'type',
    // 'other_type',
    'problem',
    // 'status',
    // 'attach',
    'description',
    'item_type_id'
  ];

  public function flies()
  {
    return $this->morphMany('App\Fly', 'fileable');
  }

  public function repair()
  {
    return $this->belongsTo(Repair::class);
  }

  public function item_type()
  {
    return $this->belongsTo(ItemType::class);
  }

  public function getTypeNameAttribute()
  {
    $name = [
      'vehicle' => 'ยานพาหนะ',
      'electronics' => 'เครื่องใช้ไฟฟ้า/เครื่องใช้ในครัวเรือน',
      'machine' => 'เครื่องมือเครื่องจักรการเกษตร',
      'other' => 'อื่นๆ'
    ];

    return $name[$this->type];
  }
}
