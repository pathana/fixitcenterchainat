<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class Post extends Model
{
  protected $fillable = ['title', 'description', 'type'];

  public function scopeAdvertise($query)
  {
    return $query->where('type', 'advertise');
  }

  public function scopeActivities($query)
  {
    return $query->where('type', 'activities');
  }

  public function scopeInformation($query)
  {
    return $query->where('type', 'information');
  }

  public function flies()
  {
    return $this->morphMany('App\Fly', 'fileable');
  }

  public function getFilePathAttribute()
  {
    return 'upload/posts/';
  }

  public function getTypeNameAttribute()
  {
    $name = [
      'advertise' => 'ข่าวประชาสัมพันธ์',
      'activities' => 'ภาพกิจกรรม',
      'information' => 'บริการข้อมูล'
    ];

    return $name[$this->type];
  }

  public function getPublicPathAttribute()
  {
    return getPublicPathPost();
  }

  public function delete()
  {
    $fileID = $this->flies->map(function($item, $key) {
        return $item->id;
    });

    \App\Fly::destroy($fileID);


    return parent::delete();
  }

}
