<?php

namespace App;

use App\ItemType;
use Illuminate\Database\Eloquent\Model;

class ItemType extends Model
{
  protected $fillable = ['name', 'code'];
    //

  public function items()
  {
    return $this->hasMany(Item::class);
  }
}
