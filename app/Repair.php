<?php

namespace App;

use App\SparePart;
use App\Http\Resources\SparePartResource;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class Repair extends Model
{
  protected $fillable = [
    'center_id',
    'citizen_id',
    'cost',
    'description',
    'start_date',
    'end_date',
    'result',
    'forward',
    'receiver_name',
    'receiver_date',
    'sender_name',
    'sender_date',
    'citizen_review',
    'citizen_comment',
    'technician_review',
    'technician_comment',
    'finished'
  ];

  public function center()
  {
    return $this->belongsTo(Center::class);
  }

  public function citizen()
  {
    return $this->belongsTo(Citizen::class);
  }

  public function item()
  {
    return $this->hasOne(Item::class);
  }

  public function performer()
  {
    return $this->hasOne(Performer::class);
  }

  public function inventories()
  {
    return $this->morphMany(Inventory::class, 'inventoriable');
  }

  public function getInventoriesCollectAttribute()
  {
    $inventories = $this->inventories->groupBy('spare_part_id');
    $parts = collect([]);

    foreach ($inventories as $key => $value) {
      $part = SparePart::find($key);
      $parts->push([
        'part' => new SparePartResource($part),
        'cost' => $value->sum('cost')
      ]);
    }

    return $parts;
  }

  public function scopeWaitRepair($query)
  {
    $query = $query->has('item');
    return $query->where('result', 'wait')->where('finished', false);
  }

  public function scopeWaitSendback($query)
  {
    $query = $query->has('item');
    return $query->whereNotIn('result', ['wait'])->where('finished', false);
  }

  public function scopeFinished($query)
  {
    $query = $query->has('item');
    return $query->where('finished', true);
  }

  public function scopeNotWait($query)
  {
    $query = $query->has('item');
    return $query->whereNotIn('result', ['wait']);
  }

  public function getResultNameAttribute()
  {
    $name = [
      'wait' => 'รอซ่อม',
      'reparable' => 'ซ่อมได้',
      'irreparable' => 'ซ่อมไม่ได้',
      'forward' => 'ส่งซ่อมต่อ' . ' ' . $this->forward
    ];

    return $name[$this->result];
  }

  public function getStatusAttribute()
  {
    if ($this->result == 'wait' and $this->finished == false) return 'waitrepair';
    if ($this->result !== 'wait' and $this->finished == false) return 'waitsendback';
    if ($this->result !== 'wait' and $this->finished) return 'finished';

    return 'waitrepair';
  }

  public function getResultAttribute($value)
  {
    return $value ? $value : '';
  }

  public function getDescriptionAttribute($value)
  {
    return $value ? $value : '';
  }

  public function getForwardAttribute($value)
  {
    return $value ? $value : '';
  }

  public function getCitizenReviewAttribute($value)
  {
    return $value ? $value : '';
  }

  public function getTechnicianReviewAttribute($value)
  {
    return $value ? $value : '';
  }

  public function getCitizenCommentAttribute($value)
  {
    return $value ? $value : '';
  }

  public function getTechnicianCommentAttribute($value)
  {
    return $value ? $value : '';
  }

  public function getSenderNameAttribute($value)
  {
    return $value ? $value : '';
  }

  public function getReceiverNameAttribute($value)
  {
    return $value ? $value : '';
  }

  public function part_save($parts)
  {
    $inventories = $this->inventories;
    if ($inventories->count() == 0) $this->addNewInventories($parts);
    else $this->addInventories($parts);
  }

  public function partCollect($parts)
  {
    $id = collect([]);
    foreach ($parts as $part) { $id->put($part['part']['id'], $part['cost']); }
    return $id;
  }

  public function changePart($old, $new, $keys)
  {
    foreach ($keys as $key) {
      $oldCost = abs($old->get($key));
      $newCost = $new->get($key);
      $diffCost = $oldCost - $newCost;
      if ($diffCost == 0) continue;
      $this->newInventory($key, $diffCost);
    }
  }

  public function deletePart($old, $keys)
  {
    foreach ($keys as $key => $value) {
      $cost = abs($old->get($key));
      $this->newInventory($key, $cost);
    }
  }

  public function addPart($new, $keys)
  {
    foreach ($keys as $key => $value) {
      $cost = $new->get($key) * -1;
      $this->newInventory($key, $cost);
    }
  }

  public function addInventories($parts)
  {
    $oldPart = $this->partCollect($this->inventories_collect);
    $newPart = $this->partCollect($parts);
    $intersectPart = $oldPart->intersectByKeys($newPart);
    $intersectKeys = $intersectPart->keys()->all();
    $delPart = $oldPart->except($intersectKeys)->all();
    $addPart = $newPart->except($intersectKeys)->all();

    if ($intersectPart->isNotEmpty()) $this->changePart($oldPart, $newPart, $intersectKeys);
    if (!empty($delPart)) $this->deletePart($oldPart, $delPart);
    if (!empty($addPart)) $this->addPart($newPart, $addPart);
  }

  public function addNewInventories($parts)
  {
    foreach ($parts as $part) {
      $id = $part['part']['id'];
      $cost = $part['cost'] * -1;
      $this->newInventory($id, $cost);
    }
  }

  public function newInventory($partId, $cost)
  {
    $part = SparePart::find($partId);
    $balance = $part->balance + $cost;

    $inventory = new Inventory;
    $inventory->spare_part_id = $part->id;
    $inventory->cost = $cost;
    $inventory->balance = $balance;

    $this->inventories()->save($inventory);
  }

}
