<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class RichtextResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
      return [
        'id' => $this->id,
        'name' => $this->name,
        'description' => $this->description,
        'content' => $this->content,
        'created_at' => (string) $this->created_at,
        'updated_at' => (string) $this->updated_at,
      ];
    }

    public function withResponse($request, $response)
    {
      $response->header('X-Value', 'True');
    }
}
