<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PerformerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
      return [
        'id' => $this->id,
        'register' => $this->register,
        'assessor' => $this->assessor,
        'chief' => $this->chief,
        'sender' => $this->sender,
        'technician' => $this->technician,
        'register_date' => (string) $this->register_date,
        'assessor_date' => (string) $this->assessor_date,
        'chief_date' => (string) $this->chief_date,
        'sender_date' => (string) $this->sender_date,
      ];
    }
}
