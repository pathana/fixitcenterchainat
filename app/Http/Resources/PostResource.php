<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Fly;

class PostResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
      $photos = $this->flies()->photo()->get();
      $files = $this->flies()->file()->get();

      return [
        'id' => $this->id,
        'title' => $this->title,
        'description' => $this->description,
        'type' => $this->type,
        'photos' => FlyResource::collection($photos),
        'documents' => FlyResource::collection($files),
        'created_at' => (string) $this->created_at->format('d/m/Y'),
        'updated_at' => (string) $this->updated_at->format('d/m/Y'),
      ];
    }
}
