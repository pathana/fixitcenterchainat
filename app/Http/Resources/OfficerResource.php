<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OfficerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
      return [
        'id' => $this->id,
        'fullname' => $this->fullname,
        'citizen_number' => $this->citizen_number,
        'number_format' => $this->number_format,
        'avatar' => $this->avatar,
        'phonenumber' => $this->phonenumber,
        'phone_format' => $this->phone_format,
        'type' => $this->type,
        'type_name' => $this->type_name,
        'department' => $this->department,
        'created_at' => (string) $this->created_at->format('d/m/Y'),
        'updated_at' => (string) $this->updated_at->format('d/m/Y'),
      ];
    }
}
