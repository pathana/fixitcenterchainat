<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class InventoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
      return [
        'id' => $this->id,
        'spare_part_id' => $this->spare_part,
        'part' => new SparePartResource($this->spare_part),
        'cost' => $this->cost,
        'balance' => $this->balance,
        'description' => $this->description,
        'inventoriable' => $this->inventoriable,
        'performer' => $this->performer,
        'created_at' => (string) $this->created_at,
        'updated_at' => (string) $this->updated_at,
      ];
    }
}
