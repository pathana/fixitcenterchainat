<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ItemResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
      $photos = $this->flies()->photo()->get();

      return [
        'id' => $this->id,
        'name' => $this->name,
        'type' => $this->type,
        'type_name' => $this->type_name,
        'other_type' => $this->other_type,
        'problem' => $this->problem,
        'status' => $this->status,
        'attach' => $this->attach,
        'description' => $this->description,
        'item_type' => new ItemTypeResource($this->item_type),
        'photos' => FlyResource::collection($photos),
      ];
    }
}
