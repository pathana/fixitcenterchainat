<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Carbon\Carbon;

class BuildResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
      $citizens = $this->peoples()->citizen()->get();
      $officers = $this->peoples()->officer()->get();

      return [
        'id' => $this->id,
        'name' => $this->name,
        'teacher' => $this->teacher,
        'hours' => $this->hours,
        'description' => $this->description,
        'status' => $this->status,
        'status_name' => $this->status_name,
        'officers' => PeopleResource::collection($officers),
        'citizens' => PeopleResource::collection($citizens),
        'start_date' => (string) Carbon::parse($this->start_date)->format('Y-m-d'),
        'end_date' => (string) Carbon::parse($this->end_date)->format('Y-m-d'),
      ];
    }
}
