<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CitizenResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
      return [
        'id' => $this->id,
        'title' => $this->title,
        'fullname' => $this->fullname,
        'address' => $this->address,
        'number' => $this->number,
        'phone' => $this->phonenumber,
        'avatar' => $this->avatar,
        'numberFormat' => $this->number_format,
        'phoneFormat' => $this->phone_format,
        'created_at' => (string) $this->created_at->format('d/m/Y'),
        'updated_at' => (string) $this->updated_at->format('d/m/Y'),
      ];
    }
}
