<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class RepairResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
      return [
        'id' => $this->id,
        'center' => new CenterResource($this->center),
        'citizen' => new CitizenResource($this->citizen),
        'item' => new ItemResource($this->item),
        'performer' => new PerformerResource($this->performer),
        'inventories' => InventoryResource::collection($this->inventories),
        'parts' => $this->inventories_collect,
        'cost' => $this->cost,
        'description' => $this->description,
        'start_date' => $this->start_date,
        'end_date' => $this->end_date,
        'result_name' => $this->result_name,
        'result' => $this->result,
        'forward' => $this->forward,
        'receiver_name' => $this->receiver_name,
        'receiver_date' => $this->receiver_date,
        'sender_name' => $this->sender_name,
        'sender_date' => $this->sender_date,
        'citizen_review' => $this->citizen_review,
        'citizen_comment' => $this->citizen_comment,
        'technician_review' => $this->technician_review,
        'technician_comment' => $this->technician_comment,
        'finished' => $this->finished,
        'status' => $this->status,
        'created_at' => (string) $this->created_at,
        'updated_at' => (string) $this->updated_at,
      ];
    }
}
