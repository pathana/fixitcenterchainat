<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AdminController extends Controller
{
  public function __construct()
  {
    $this->middleware('auth');
  }

  public function index(Request $request)
  {
    $user = $request->user();
    if ($user->hasAnyRole(['admin', 'officer'])) {
      return view('admin.dashboard');
    }

    return redirect('/home');
  }

  public function center(Request $request)
  {
    return view('center.dashboard');
  }

  public function officer(Request $request)
  {
    $user = $request->user();
    if ($user->hasAnyRole(['admin', 'officer'])) {
      return view('officer.dashboard');
    }

    return redirect('/home');
  }

  public function inventories(Request $request)
  {
    $user = $request->user();
    if ($user->role_name == 'student') return redirect('/home');
    return view('inventories.dashboard');
  }
}
