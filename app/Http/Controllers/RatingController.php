<?php

namespace App\Http\Controllers;

use App\Rating;
use Illuminate\Http\Request;

class RatingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $sex_array = Rating::getStats('sex');
      $age_array = Rating::getStats('age');
      $career_array = Rating::getStats('career');
      $education_array = Rating::getStats('education');
      $rating_stats = Rating::ratingStats();

      return view('ratings.index', [
        'sexArray' => $sex_array,
        'ageArray' => $age_array,
        'careerArray' => $career_array,
        'educationArray' => $education_array,
        'peoples' => Rating::count(),
        'ratingStats' => $rating_stats
      ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      return view('ratings.create');
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $valid = $this->validate($request, [
        'sex' => 'required',
        'age' => 'required',
        'career' => 'required',
        'education' => 'required',
        'option1' => 'required',
        'option2' => 'required',
        'option3' => 'required',
        'option4' => 'required',
        'option5' => 'required',
        'option6' => 'required',
        'option7' => 'required',
      ]);

      $request['sum_option'] = 0;

      for($i = 1; $i <= 7; $i++) $request['sum_option'] += $request['option'.$i];

      Rating::create($request->all());

      return view('ratings.create');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Rating  $rating
     * @return \Illuminate\Http\Response
     */
    public function show(Rating $rating)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Rating  $rating
     * @return \Illuminate\Http\Response
     */
    public function edit(Rating $rating)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Rating  $rating
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Rating $rating)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Rating  $rating
     * @return \Illuminate\Http\Response
     */
    public function destroy(Rating $rating)
    {
        //
    }
}
