<?php

namespace App\Http\Controllers;

use App\Build;
use Illuminate\Http\Request;
use App\Http\Resources\BuildResource;
use Illuminate\Support\Facades\Input;

class BuildController extends Controller
{
    public function __construct()
    {
      $this->middleware('jwt.auth')->except(['index', 'show']);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $search = Input::get('search');
      $builds = Build::latest();

      if (!empty($search)) {
        $builds = $builds->where('name', 'like', "%{$search}%")
                          ->orWhere('teacher', 'like', "%{$search}%");
      }

      return BuildResource::collection($builds->paginate(12));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $valid = $this->validate($request, [
        'name' => 'required',
        'teacher' => 'required',
        'hours' => 'required',
        'start_date' => 'required',
        'end_date' => 'required'
      ]);

      $build = Build::create([
        'name' => $request->name,
        'teacher' => $request->teacher,
        'hours' => $request->hours,
        'start_date' => \Carbon\Carbon::parse($request->start_date),
        'end_date' => \Carbon\Carbon::parse($request->end_date),
      ]);

      return new BuildResource($build);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Build  $build
     * @return \Illuminate\Http\Response
     */
    public function show(Build $build)
    {
      return new BuildResource($build);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Build  $build
     * @return \Illuminate\Http\Response
     */
    public function edit(Build $build)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Build  $build
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Build $build)
    {
      $valid = $this->validate($request, [
        'name' => 'required',
        'teacher' => 'required',
        'hours' => 'required',
        'start_date' => 'required',
        'end_date' => 'required'
      ]);

      $data = $request->except('_token');

      foreach ($data as $key => $value) {
        $build[$key] = $value;
      }

      $build->save();

      return new BuildResource($build);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Build  $build
     * @return \Illuminate\Http\Response
     */
    public function destroy(Build $build)
    {
        //
    }
}
