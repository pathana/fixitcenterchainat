<?php

namespace App\Http\Controllers;

use App\Post;
use App\Fly;
use Illuminate\Http\Request;
use App\Http\Resources\PostResource;
use App\Http\Resources\FlyResource;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Input;
use Faker\Factory;
use Faker\Provider\Miscellaneous;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;

class AdvertiseController extends Controller
{
    public function __construct()
    {
      $this->middleware('jwt.auth')->except(['index', 'show']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $search = [
        'title' => Input::get('search'),
        'from' => Input::get('startDate'),
        'to' => Input::get('endDate'),
      ];

        return $this->search($search);
    }

    public function search($search)
    {
        $title = $search['title'];
        $from = (!empty($search['from']) ? Carbon::parse($search['from']) : "");
        $to = (!empty($search['to']) ? Carbon::parse($search['to']) : "");

        // $post = Post::latest()->where('type', 'advertise');
        $post = Post::advertise()->latest();

        if (!empty($title)) {
            $post = $post->where('title', 'like', "%{$title}%");
        }


        if (!empty($from) && !empty($to)) {
            $post = $post
                  ->whereDate('updated_at', '>=', $from)
                  ->whereDate('updated_at', '<=', $to);
        }

        return PostResource::collection($post->paginate(12));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
          'title' => 'required',
          'description' => 'required',
        ]);

        $post = Post::create([
          'title' => $request->title,
          'description' => $request->description,
          'type' => 'advertise'
        ]);

        return new PostResource($post);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return new PostResource(Post::find($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
          'title' => 'required',
          'description' => 'required',
          'fileDelete' => 'array'
        ]);

        $post = Post::find($id)->update($request->only(['title', 'description']));

        if (!empty($request->fileDelete)) {
          foreach ($request->fileDelete as $file) {
            Fly::find($file)->deletePath(getPublicPathPost());
          }
        }

        return response()->json(['message' => 'update Successfully']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::find($id);
        $post->delete();

        return response()->json(null, 204);
    }
}
