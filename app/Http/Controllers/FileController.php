<?php

namespace App\Http\Controllers;

use App\Post;
use App\Fly;
use App;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class FileController extends Controller
{
    public function __construct()
    {
      $this->middleware('jwt.auth')->except(['index', 'show']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $this->validate($request, [
        'id' => 'required|numeric',
        'type' => 'required',
        'files' => 'required|array',
        'class' => 'required'
      ]);

      $errors = [];

      $model = '\App\\' . $request->type;
      $post = $model::find($request->id);

      if (!isset($post)) $errors['id'] = "model not found";
      if ($request->hasFile('files') == false) $errors['files'] = 'files not found';
      if (!empty($errors)) return response()->json(['errors' => json_encode($errors)], 422);

      $faker = \Faker\Factory::create();
      $files = $request->file('files');
      $lists = collect([]);

      foreach ($files as $file) {
        $extension = 'file';
        if (Fly::isPhoto($file->getClientOriginalExtension())) $extension = 'photo';

        $name = $faker->md5.'.'.$file->getClientOriginalExtension();
        $destination = getPublicPathName($request->class) . $name;
        if ($extension == 'photo') Fly::compressImage($file, $destination);
        else $file->move(getPublicPathName($request->class), $name);

        $fileNew = new Fly;
        $fileNew->name = $file->getClientOriginalName();
        $fileNew->destination = $name;
        $fileNew->extension = $extension;
        $lists->push($fileNew);
      }

      $post->flies()->saveMany($lists);

      return response()->json(['message' => 'Successfully Photo Upload']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
