<?php

namespace App\Http\Controllers;

use App\Item;
use App\ItemSuggestion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use App\Http\Resources\ItemSuggestionResource;
use App\Http\Resources\RepairResource;

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function suggestion(Request $request)
    {
      $search = Input::get('search');
      $item_suggestion = ItemSuggestion::where('name', 'like', "%{$search}%");

      return ItemSuggestionResource::collection($item_suggestion->get());
    }

    public function suggestions(Request $request)
    {
      return ItemSuggestionResource::collection(ItemSuggestion::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function show(Item $item)
    {
      $repair = $item->repair;

      return new RepairResource($repair);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function edit(Item $item)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Item $item)
    {
      $valid = $this->validate($request, [
        'citizen' => 'required',
        'register' => 'required',
        'item' => 'required',
        'deletePhoto' => 'array'
      ]);

      $repair = $item->repair;
      $newItem = new \App\Item($request->item);
      $repair->citizen_id = $request->citizen['id'];
      $repair->performer->register = $request->register['id'];
      $repair->performer->save();
      $repair->save();

      $keys = $item->getFillable();

      foreach ($keys as $key) {
        if (is_array($newItem[$key])) $newItem[$key] = implode(",", $newItem[$key]);
        if (is_string($newItem[$key])) ItemSuggestion::add_name($newItem[$key]);
        $item[$key] = $newItem[$key];
      }
      $item->save();

      if (!empty($request->deletePhoto))
        \App\Fly::destroy($request->deletePhoto);

      return new RepairResource($repair);
    }

    public function item_return(Request $request, Item $item)
    {
      $valid = $this->validate($request, [
        'receiver_name' => 'required',
        'sender_name' => 'required',
        'citizen_review' => 'required|numeric',
        'technician_review' => 'required|numeric'
      ]);

      $repair = $item->repair;
      $repair->update($request->only([
        'receiver_name', 'sender_name',
        'citizen_review', 'citizen_comment',
        'technician_review', 'technician_comment',
      ]));
      $repair->receiver_date = \Carbon\Carbon::now();
      $repair->sender_date = \Carbon\Carbon::now();
      $repair->finished = true;
      $repair->save();

      return new RepairResource($repair);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function destroy(Item $item)
    {
        //
    }
}
