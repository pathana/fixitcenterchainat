<?php

namespace App\Http\Controllers;

use App\Repair;
use App\Citizen;
use App\Center;
use App\Officer;
use App\Performer;
use App\Item;
use App\ItemSuggestion;
use App\SparePart;
use App\Inventory;
use PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use App\Http\Resources\RepairResource;
use App\Http\Requests\RepairStoreRequest;
use App\Http\Requests\RepairUpdateRequest;

class RepairController extends Controller
{
    public function __construct()
    {
      $this->middleware('jwt.auth')->except([
        'index',
        'show',
        'maincenter',
        'printable',
        'center',
        'wait_repair',
        'wait_sendback',
        'finished',
        'not_wait'
      ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    public function maincenter()
    {
      $repairs = Repair::where('center_id', 1)->latest();
      return RepairResource::collection($repairs->paginate(12));
    }

    public function center($id)
    {
      $repairs = Repair::where('center_id', $id)->latest();
      return RepairResource::collection($repairs->paginate(12));
    }

    public function wait_repair($center_id=null)
    {
      $repairs = Repair::WaitRepair()->latest();
      if ($center_id !== null) $repairs = $repairs->where('center_id', $center_id)->latest();

      return RepairResource::collection($repairs->paginate(12));
    }

    public function wait_sendback($center_id=null)
    {
      $repairs = Repair::WaitSendback()->latest();
      if ($center_id !== null) $repairs = $repairs->where('center_id', $center_id)->latest();

      return RepairResource::collection($repairs->paginate(12));
    }

    public function finished($center_id=null)
    {
      $repairs = Repair::Finished()->latest();
      if ($center_id !== null) $repairs = $repairs->where('center_id', $center_id)->latest();

      return RepairResource::collection($repairs->paginate(12));
    }

    public function not_wait($center_id=null, $date)
    {
      $repairs = Repair::NotWait()->latest();
      if ($center_id !== null) $repairs = $repairs->where('center_id', $center_id)->latest();
      $repairs = $repairs->whereDate('created_at', $date);

      return RepairResource::collection($repairs->get());
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RepairStoreRequest $request)
    {
      $validated = $request->validated();

      $center = Center::find($request->center_id);
      $citizen = Citizen::find($request->citizen_id);
      $officer = Officer::find($request->officer_id);

      $errors = [];

      if (!isset($center)) $errors['center'] = 'center not found';
      if (!isset($citizen)) $errors['citizen'] = 'citizen not found';
      if (!isset($officer)) $errors['officer'] = 'officer not found';
      if (!empty($errors)) return response()->json(['errors' => json_encode($errors)], 422);

      // ItemSuggestion::add_name($request->itemName);
      ItemSuggestion::add_name($request->itemProblem);
      ItemSuggestion::add_name($request->itemDescription);

      $item = new Item;
      $item->item_type_id = $request->item_type_id;
      $item->problem = $request->itemProblem;
      $item->description = $request->itemDescription;

      $performer = new Performer;
      $performer->register = $request->officer_id;
      $performer->register_date = \Carbon\Carbon::now();

      $repair = new Repair;
      $repair->center_id = $request->center_id;
      $repair->citizen_id = $request->citizen_id;
      $repair->start_date = \Carbon\Carbon::now();
      $repair->result = 'wait';
      $repair->save();
      $repair->item()->save($item);
      $repair->performer()->save($performer);

      return new RepairResource($repair);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Repair  $repair
     * @return \Illuminate\Http\Response
     */
    public function show(Repair $repair)
    {
      return new RepairResource($repair);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Repair  $repair
     * @return \Illuminate\Http\Response
     */
    public function edit(Repair $repair)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Repair  $repair
     * @return \Illuminate\Http\Response
     */
    public function update(RepairUpdateRequest $request, Repair $repair)
    {
      $validated = $request->validated();
      $keys = ['result', 'forward', 'cost'];
      $performer_keys = ['assessor', 'chief', 'sender'];
      $performer = $repair->performer;
      $description = $request->description;
      if (is_array($request->description)) $description = implode(",", $request->description);

      $repair->description = $description;
      $performer->technician = $request->technician;

      foreach ($keys as $key) { $repair[$key] = $request[$key]; }
      foreach ($performer_keys as $key) {
        $performer[$key] = $request[$key];
        $performer[$key . '_date'] = \Carbon\Carbon::now();
      }

      $repair->performer->save();
      $repair->end_date = \Carbon\Carbon::now();
      $repair->save();

      $repair->part_save($request->parts);
      ItemSuggestion::add_name($description);
      return new RepairResource($repair);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Repair  $repair
     * @return \Illuminate\Http\Response
     */
    public function destroy(Repair $repair)
    {
        //
    }

    public function printable($id)
    {
      $repair = Repair::find($id);
      $center = $repair->center;
      $data = [ 'repair' => $repair ];
      $name = 'repair-' . $id . '.pdf';
      $pdfPath = 'pdf.slip';
      $itemPath = url('finditem') . "?id=" . $repair->item->id;
      $qrcode = \QrCode::format('png')->size(80)->generate($itemPath, public_path('upload/qrcode') . '/' . $repair->item->id);
      $config = [];

      $pdf = PDF::loadView($pdfPath, $data, [], $config);
      return $pdf->stream($name);
    }
}
