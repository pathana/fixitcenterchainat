<?php

namespace App\Http\Controllers;

use App\Center;
use App\Item;
use App\Citizen;
use Illuminate\Http\Request;
use App\Http\Resources\CenterResource;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Validator;

class CenterController extends Controller
{

    public function __construct()
    {
      $this->middleware('jwt.auth')->except(['index', 'show', 'report']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $search = Input::get('search');
      $centers = Center::latest();

      if (!empty($search))
        $centers = $centers->where('name', 'like', "%{$search}%")
                      ->orWhere('amphoe', 'like', "%{$search}%")
                      ->orWhere('province', 'like', "%{$search}%");

      return CenterResource::collection($centers->paginate(12));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $valid = $this->validate($request, [
        'name' => 'required',
        'amphoe' => 'required',
        'province' => 'required'
      ]);


      $center = Center::create([
        'name' => $request->name,
        'amphoe' => $request->amphoe,
        'province' => $request->province
      ]);

      return new CenterResource($center);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Center  $center
     * @return \Illuminate\Http\Response
     */
    public function show(Center $center)
    {
      return new CenterResource($center);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Center  $center
     * @return \Illuminate\Http\Response
     */
    public function edit(Center $center)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Center  $center
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Center $center)
    {
      $valid = $this->validate($request, [
        'name' => 'required',
        'amphoe' => 'required',
        'province' => 'required'
      ]);

      $center->update($request->only(['name', 'amphoe', 'province']));

      return new CenterResource($center);
    }

    public function report(Request $request, Center $center)
    {
      $items = Item::whereHas('repair', function ($query) use($center){
          $query->where('center_id', $center->id);
      });

      $item_group = $items->groupBy('item_type_id')->select('item_type_id', DB::raw('count(*) as total'));

      $citizens = Citizen::whereHas('repairs', function ($query) use($center) {
        $query->where('center_id', $center->id)->distinct()->select('citizen_id');
      });

      return response()->json([
        'itemGroup' => $item_group->get(),
        'citizenTotal' => $citizens->count()
      ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Center  $center
     * @return \Illuminate\Http\Response
     */
    public function destroy(Center $center)
    {
      if ($center->id != 1)
        $center->delete();

      return response()->json(null, 204);
    }

    public function dashboard(Request $request)
    {
      return view('center.dashboard');
    }
}
