<?php

namespace App\Http\Controllers;

use App\Inventory;
use App\SparePart;
use App\User;
use Importer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Http\Resources\InventoryResource;

class InventoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function last($part_id)
    {
      $inventory = Inventory::where('spare_part_id', $part_id)->latest()->first();
      if (empty($inventory)) $inventory = new Inventory;

      return new InventoryResource($inventory);
    }

    public function part($part_id)
    {
      $inventories = Inventory::where('spare_part_id', $part_id)->latest();

      return InventoryResource::collection($inventories->paginate(12));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $valid = $this->validate($request, [
        'cost' => 'required|numeric',
        'part_id' => 'required|numeric',
        'user_id' => 'required|numeric'
      ]);

      $part = SparePart::find($request->part_id);
      $balance = $part->balance + $request->cost;

      $inventory = new Inventory;
      $inventory->spare_part_id = $part->id;
      $inventory->cost = $request->cost;
      $inventory->balance = $balance;

      $user = User::find($request->user_id);
      $user->inventories()->save($inventory);

      return new InventoryResource($inventory);
    }

    public function upload(Request $request)
    {
      $valid = $this->validate($request, [
        'file' => 'File|mimes:xlsx',
        'department_id' => 'numeric'
      ]);

      $path = $request->file('file')->getRealPath();
      $excel = Importer::make('Excel')->load($path);
      $collection = $excel->getCollection();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Inventory  $inventory
     * @return \Illuminate\Http\Response
     */
    public function show(Inventory $inventory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Inventory  $inventory
     * @return \Illuminate\Http\Response
     */
    public function edit(Inventory $inventory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Inventory  $inventory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Inventory $inventory)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Inventory  $inventory
     * @return \Illuminate\Http\Response
     */
    public function destroy(Inventory $inventory)
    {
        //
    }
}
