<?php

namespace App\Http\Controllers;

use App\SparePart;
use App\Http\Resources\SparePartResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Input;

class SparePartController extends Controller
{
    public function __construct()
    {
      $this->middleware('jwt.auth')->except([
        'index', 'show', 'department', 'categories'
      ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $department = Input::get('department');
      $category = Input::get('category');
      $search = Input::get('search');
      $parts = SparePart::latest();

      if (!empty($department)) $parts = $parts->where('department_id', $department);
      if (!empty($category)) $parts = $parts->where('category', $category);
      if (!empty($search)) $parts = $parts->where('name', 'like', "%{$search}%");

      return SparePartResource::collection($parts->paginate(12));
    }

    public function dashboard(Request $request)
    {
      $user = $request->user();
      return view('sparepart.dashboard');
    }

    public function department($id = null)
    {
      if ($id == null) return [];
      $category = Input::get('category');
      $parts = SparePart::where('department_id', $id)->latest();

      if (!empty($category))
        $parts = $parts->where('category', $category);

      return SparePartResource::collection($parts->paginate(12));
    }

    public function categories($id)
    {
      $categories = SparePart::distinct();
      if (is_numeric($id)) $categories = $categories->where('department_id', $id);
      $categories = $categories->select('category');

      return response()->json([
        'categories' => $categories->get()
      ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $valid = $this->validate($request, [
        'name' => 'required',
        'department_id' => 'required|numeric',
        'category' => 'required',
        'price' => 'required|numeric'
      ]);

      $part= new SparePart;
      $data = $request->except('_token');

      foreach ($data as $key => $value) {
        $part[$key] = $value;
      }

      $part->save();

      return new SparePartResource($part);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SparePart  $sparePart
     * @return \Illuminate\Http\Response
     */
    public function show(SparePart $sparePart)
    {
      return new SparePartResource($sparePart);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SparePart  $sparePart
     * @return \Illuminate\Http\Response
     */
    public function edit(SparePart $sparePart)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SparePart  $sparePart
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SparePart $sparePart)
    {
      $valid = $this->validate($request, [
        'name' => 'required',
        'category' => 'required',
        'price' => 'required|numeric'
      ]);

      $part= $sparePart;
      $data = $request->except('_token');

      foreach ($data as $key => $value) {
        $part[$key] = $value;
      }

      $part->save();

      return new SparePartResource($part);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SparePart  $sparePart
     * @return \Illuminate\Http\Response
     */
    public function destroy(SparePart $sparePart)
    {
        //
    }
}
