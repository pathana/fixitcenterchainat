<?php

namespace App\Http\Controllers;

use App\Officer;
use App\Http\Resources\OfficerResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;


class OfficerController extends Controller
{
    public function __construct()
    {
      $this->middleware('jwt.auth')->except([
        'index',
        'show',
        'search',
        'all'
      ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $type = Input::get('type');
      $department_id = Input::get('department_id');
      $officers = Officer::latest();

      if (!empty($type))
        $officers = $officers->where('type', $type);

      if (!empty($department_id))
        $officers = $officers->where('department_id', $department_id);

      return OfficerResource::collection($officers->paginate(12));
    }

    public function search(Request $request)
    {
      $search = Input::get('search');
      $officers = Officer::latest();

      if (!empty($search))
        $officers = $officers->where('fullname', 'like', "%{$search}%")
                      ->orWhere('citizen_number', 'like', "%{$search}%")
                      ->orWhere('phonenumber', 'like', "%{$search}%");

      return OfficerResource::collection($officers->get());
    }

    public function all()
    {
      $officers = Officer::all();
      return OfficerResource::collection($officers);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $valid = $this->validate($request, [
        'fullname' => 'required',
        'citizen_number' => 'required|max:13|min:13|unique:officers,citizen_number',
        'phonenumber' => 'required',
        'department_id' => 'required|numeric',
        'type' => 'required'
      ]);

      $officer = Officer::create([
        'fullname' => $request->fullname,
        'citizen_number' => $request->citizen_number,
        'phonenumber' => $request->phonenumber,
        'department_id' => $request->department_id,
        'type' => $request->type,
      ]);

      return new OfficerResource($officer);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Officer  $officer
     * @return \Illuminate\Http\Response
     */
    public function show(Officer $officer)
    {
      return new OfficerResource($officer);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Officer  $officer
     * @return \Illuminate\Http\Response
     */
    public function edit(Officer $officer)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Officer  $officer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Officer $officer)
    {
      $valid = $this->validate($request, [
        'fullname' => 'required',
        'citizen_number' => 'required|max:13|min:13',
        'phonenumber' => 'required',
        'department_id' => 'required|numeric',
        'type' => 'required'
      ]);

      $officer->update(
        $request->only([
          'fullname',
          'citizen_number',
          'phonenumber',
          'department_id',
          'type'
        ])
      );

      return new OfficerResource($officer);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Officer  $officer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Officer $officer)
    {
        //
    }
}
