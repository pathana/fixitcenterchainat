<?php

namespace App\Http\Controllers;

use App\People;
use App\Build;
use Illuminate\Http\Request;
// use App\Http\Resources\PeopleResource;
use App\Http\Resources\BuildResource;

class PeopleController extends Controller
{
    public function __construct()
    {
      $this->middleware('jwt.auth')->except(['index', 'show']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $valid = $this->validate($request, [
        'type' => 'required',
        'person_id' => 'required|numeric',
        'build_id' => 'required|numeric'
      ]);

      $build = Build::find($request->build_id);
      $person_key = $request->type . '_id';
      $person = $build->peoples()->where($person_key, $request->person_id);
      $errors = [];

      if ($person->count() > 0) {
        $errors['duplicate'] = 'มีในระบบอยู่แล้ว';
        return response()->json(['errors' => json_encode($errors)], 422);
      }

      $people = new People;
      $people->type = $request->type;
      if ($request->type == 'citizen') $people->citizen_id = $request->person_id;
      if ($request->type == 'officer') $people->officer_id = $request->person_id;
      $build->peoples()->save($people);
      return new BuildResource($build);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\People  $people
     * @return \Illuminate\Http\Response
     */
    public function show(People $people)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\People  $people
     * @return \Illuminate\Http\Response
     */
    public function edit(People $people)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\People  $people
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, People $people)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\People  $people
     * @return \Illuminate\Http\Response
     */
    public function destroy(People $people)
    {
      $people->delete();

      return response()->json(null, 204);
    }
}
