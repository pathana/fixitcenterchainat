<?php

namespace App\Http\Controllers;

use App\User;
use JWTAuth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;

class AuthController extends Controller
{
  public function __construct()
  {
    $this->middleware('jwt.auth')->except(['authenticate', 'login']);
  }

  public function authenticate(Request $request)
  {
    $credentials = $request->only('email', 'password');

    if (! $token = JWTAuth::attempt($credentials)) {
      return response()->json(['error' => 'invalid_credentials'], 400);
    }

    return $this->respondWithToken($token);
  }

  public function login(Request $request)
  {
    $user = User::find($request->userId);

    $apiToken = auth('api')->login($user);

    if (! $apiToken) {
      return response()->json(['error' => 'Unauthorized'], 401);
    }

    return $this->respondWithToken($apiToken);
  }

  public function user()
  {
    return response()->json(auth('api')->user());
  }

  public function logout()
  {
    auth('api')->logout();

    return response()->json(['message' => 'Successfully logged out']);
  }

  public function refresh()
  {
    return $this->respondWithToken(auth('api')->refresh());
  }

  protected function respondWithToken($token)
  {
    return response()->json([
      'access_token' => $token,
      'token_type' => 'bearer',
      'expires_in' => auth('api')->factory()->getTTL() * 60,
    ]);
  }
}
