<?php

namespace App\Http\Controllers;

use App\Post;
use App\Fly;
use Illuminate\Http\Request;
use App\Http\Resources\PostResource;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Input;

class PostController extends Controller
{

    public function __construct()
    {
      $this->middleware('jwt.auth')->except(['index', 'show']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $search = [
        'type' => Input::get('type'),
        'title' => Input::get('search')
      ];

      return $this->search($search);
    }

    public function search($search)
    {
      $type = $search['type'];
      $title = $search['title'];

      $post = Post::where('type', $type)->latest();

      if (!empty($title))
        $post = $post->where('title', 'like', "%{$title}%");

      return PostResource::collection($post->paginate(12));
    }

    public function advertise()
    {
      return PostResource::collection(Post::latest()->paginate(5));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
          'title' => 'required',
          'description' => 'required',
          'type' => 'required'
        ]);

        $post = Post::create([
          'title' => $request->title,
          'description' => $request->description,
          'type' => $request->type
        ]);

        return new PostResource($post);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
      return new PostResource($post);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        $this->validate($request, [
          'title' => 'required',
          'description' => 'required',
          'fileDelete' => 'array',
          'type' => 'required'
        ]);

        $post->update($request->only(['title', 'description']));

        if (!empty($request->fileDelete))
          Fly::destroy($request->fileDelete);

        return response()->json(['message' => 'update Successfully']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
      $post->delete();

      return response()->json(null, 204);
    }

    public function all()
    {
      return view('landing', [
        'posts' => Post::latest()->paginate(5)
      ]);
    }

    public function single(Post $post)
    {
      return view('single', compact('post'));
    }
}
