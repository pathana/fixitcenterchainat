<?php

namespace App\Http\Controllers;

use App\Richtext;
use Illuminate\Http\Request;
use App\Http\Resources\RichtextResource;
use Illuminate\Support\Facades\Log;

class RichtextController extends Controller
{
    public function __construct()
    {
      $this->middleware('jwt.auth')->except(['index', 'show']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      return RichtextResource::collection(Richtext::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Richtext $richtext)
    {
      return new RichtextResource($richtext);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Richtext $richtext)
    {
      $this->validate($request, [
        'content' => 'required',
      ]);

      $richtext->update($request->only(['content']));

      return (new RichtextResource($richtext))
        ->response()
        ->header('X-Value', 'True');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
