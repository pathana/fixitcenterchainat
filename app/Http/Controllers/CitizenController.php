<?php

namespace App\Http\Controllers;

use App\Citizen;
use App\Http\Resources\CitizenResource;
use App\Http\Resources\RepairResource;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use App\Http\Requests\CitizenStoreRequest;

class CitizenController extends Controller
{
    public function __construct()
    {
      $this->middleware('jwt.auth')->except(['index', 'show', 'search', 'repairs']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $search = Input::get('search');
      $citizens = Citizen::latest();

      if (!empty($search))
        $citizens = $citizens->where('fullname', 'like', "%{$search}%")
                      ->orWhere('number', 'like', "%{$search}%")
                      ->orWhere('phonenumber', 'like', "%{$search}%");

      return CitizenResource::collection($citizens->paginate(12));
    }

    public function search(Request $request)
    {
      $search = Input::get('search');
      $citizens = Citizen::latest();

      if (!empty($search))
        $citizens = $citizens->where('fullname', 'like', "%{$search}%")
                      ->orWhere('number', 'like', "%{$search}%")
                      ->orWhere('phonenumber', 'like', "%{$search}%");

      return CitizenResource::collection($citizens->get());
    }

    public function repairs(Request $request, Citizen $citizen)
    {
      $repairs = $citizen->repairs;

      return RepairResource::collection($repairs);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CitizenStoreRequest $request)
    {
      $validated = $request->validated();

      $faker = \Faker\Factory::create();

      $name = $faker->md5.'.'.$request->avatar->getClientOriginalExtension();
      $destination = getPublicPathCitizen() . $name;
      \App\Fly::compressImage($request->avatar, $destination);

      $citizen = new Citizen;
      $citizen->title = $request->title;
      $citizen->fullname = $request->fullname;
      $citizen->address = $request->address;
      $citizen->number = $request->number;
      $citizen->phonenumber = $request->phone;
      $citizen->avatar = $name;

      $citizen->save();

      return new CitizenResource($citizen);
    }

    public function store_api(Request $request)
    {
      $valid = $this->validate($request, [
        'title' => 'required',
        'fullname' => 'required',
        'address' => 'required',
        'number' => 'required|max:13|min:13',
        'phonenumber' => 'required|max:10',
        'avatar' => 'required'
      ]);

      $find_citizen = Citizen::where('number', $request->number)->first();
      if (isset($find_citizen)) return $find_citizen->id;

      $image_base64 = base64_decode($request->avatar);
      $name = uniqid() . '.png';
      $destination = getPublicPathCitizen() . $name;
      file_put_contents($destination, $image_base64);

      $citizen = Citizen::create([
        'title' => $request->title,
        'fullname' => $request->fullname,
        'address' => $request->address,
        'number' => $request->number,
        'phonenumber' => $request->phonenumber,
        'avatar' => $name
      ]);

      return $citizen->id;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Citizen $citizen)
    {
      return new CitizenResource($citizen);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Citizen $citizen)
    {
      $valid = $this->validate($request, [
        'title' => 'required',
        'fullname' => 'required',
        'address' => 'required',
        'phonenumber' => 'required|max:10',
      ]);

      $citizen->update($request->only(['title', 'fullname', 'address', 'number', 'phonenumber']));

      return new CitizenResource($citizen);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @param \App\Citizen $citizen
     * @return \Illuminate\Http\Response
     */
    public function destroy(Citizen $citizen)
    {
      $citizen->delete();

      return response()->json(null, 204);
    }

}
