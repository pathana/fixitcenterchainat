<?php

namespace App\Http\Controllers;

use App\Richtext;
use App\Post;
use App\Fly;
use App\Item;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Input;

class LandingController extends Controller
{
  public function mission(Request $request)
  {
    return view('mission', [
      'mission' => Richtext::where('name', 'mission')->first()
    ]);
  }

  public function personnel(Request $request)
  {
    return view('personnel', [
      'personnel' => Richtext::where('name', 'personnel')->first()
    ]);
  }

  public function rating(Request $request)
  {
    return view('rating');
  }

  public function index(Request $request)
  {
    return view('welcome', [
      'advertises' => Post::advertise()->latest()->paginate(5),
      'activities' => Post::activities()->latest()->paginate(5),
      'informations' => Post::information()->latest()->paginate(5)
    ]);
  }

  public function postSingle($id)
  {
    return view('post', [
      'post' => Post::find($id)
    ]);
  }

  public function itemFind(Request $request)
  {
    $itemId = Input::get('id');
    $item = Item::find($itemId);
    $repair = $item->repair;

    return view('item_find', [
      'repair' => $repair
    ]);
  }

  public function saveRating(Request $request)
  {
    $rating = Input::get('rating');

    return view('rating');
  }

  public function postAll(Request $request)
  {
    $search = Input::get('search');
    $month = Input::get('month');
    $year = Input::get('year');
    $type = Input::get('type');
    if (empty($type)) $type = 'advertise';

    $post = Post::where('type', $type)->latest();

    if ($search != "")
      $post = $post->where('title', 'like', "%{$search}%");

    if (!empty($month))
      $post = $post->whereMonth('created_at', $month);

    if (!empty($year))
      $post = $post->whereYear('created_at', $year);


    $post = $post->paginate(6);
    $params = array(
      'search' => $search,
      'month' => $month,
      'year' => $year,
      'type' => $type
    );
    $pagination = $post->appends($params);

    $type_name = [
      'advertise' => 'ข่าวประชาสัมพันธ์',
      'activities' => 'ภาพกิจกรรม',
      'information' => 'บริการข้อมูล'
    ];

    return view('posts', [
      'posts' => $post,
      'oldSearch' => $search,
      'oldYear' => $year,
      'oldMonth' => $month,
      'postHeader' => $type_name[$type],
      'type' => $type
    ]);
  }

  public function download($id)
  {
    $file = Fly::find($id);
    $pathToFile = getPublicPathPost() . $file->destination;

    $headers = array(
      'Content-Type: ' . mime_content_type($pathToFile),
    );

    return response()->download($pathToFile, $file->name , $headers);
  }

  public function pdf()
  {
    return view('pdf');
  }
}
