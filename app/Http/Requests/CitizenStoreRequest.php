<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CitizenStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          'title' => 'required',
          'fullname' => 'required',
          'address' => 'required',
          'number' => 'required|min:13|max:13|unique:citizens,number',
          'phone' => 'required|max:10',
          'avatar' => 'required|image'
        ];
    }

    public function attributes()
    {
      return [
        'title' => 'คำนำหน้า',
        'fullname' => 'ชื่อ นามสกุล',
        'address' => 'ที่อยู่',
        'number' => 'บัตรประชาชน',
        'phone' => 'เบอร์โทรศัพท์',
        'avatar' => 'รูปภาพบัตรประชาชน',
      ];
    }
}
