<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RepairUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          'result' => 'required',
          'description' => 'required',
          'cost' => 'required|numeric',
          // 'assessor' => 'required|numeric',
          // 'chief' => 'required|numeric',
          // 'sender' => 'required|numeric',
          'technician' => 'required|numeric',
          'parts' => 'array'
        ];
    }

    public function attributes()
    {
        return [
          'result' => 'ผลการซ่อม',
          'description' => 'รายละเอียดการซ่อม',
          'cost' => 'ค่าแรง',
          // 'assessor' => 'ผู้ประเมินงานซ่อม',
          // 'chief' => 'หัวหน้าชุดซ่อม',
          // 'sender' => 'ผู้ส่งซ่อม',
          'technician' => 'ผู้ดำเนินการซ่อม',
          'parts' => 'อะไหล่'
        ];
    }
}
