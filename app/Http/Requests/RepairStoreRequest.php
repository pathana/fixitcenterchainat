<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RepairStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          'citizen_id' => 'required',
          'item_type_id' => 'required',
          'itemProblem' => 'required',
          'itemDescription' => 'required',
          'officer_id' => 'required',
          'center_id' => 'required|numeric'
        ];
    }

    public function attributes()
    {
      return [
        'citizen_id' => 'ผู้รับบริการ',
        'item_type_id' => 'สิ่งของที่นำมาซ่อม',
        'itemProblem' => 'อาการเสีย',
        'itemDescription' => 'รายละเอียด',
        'officer_id' => 'ผู้รับลงทะเบียน',
        'center_id' => 'ศูนย์'
      ];
    }
}
