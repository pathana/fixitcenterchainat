<?php

namespace App;

use App\Officer;
use Illuminate\Database\Eloquent\Model;
use App\Http\Resources\OfficerResource;

class Performer extends Model
{
  protected $fillable = [
    'register',
    'register_date',
    'assessor',
    'assessor_date',
    'chief',
    'chief_date',
    'sender',
    'sender_date',
    'technician'
  ];

  public function repair()
  {
    return $this->belongsTo(Repair::class);
  }

  public function getRegisterAttribute($value)
  {
    return $this->officer_data($value);
  }

  public function getAssessorAttribute($value)
  {
    return $this->officer_data($value);
  }

  public function getChiefAttribute($value)
  {
    return $this->officer_data($value);
  }

  public function getSenderAttribute($value)
  {
    return $this->officer_data($value);
  }

  public function getTechnicianAttribute($value)
  {
    return $this->officer_data($value);
  }

  public function officer_data($value) {
    if ($value == null) return Officer::null_data();
    return Officer::find($value);
  }
}
