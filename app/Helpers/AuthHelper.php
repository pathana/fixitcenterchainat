<?php

if (! function_exists('getPublicPath')) {
  function getPublicPath()
  {
    $url = url('/');

    if (preg_match('/fixitcenterchainat.com/', $url)) {
      $base = preg_split('/\/laravel/', base_path());
      return $base[0] . '/public_html';
    }

    return public_path();
  }
}

if (! function_exists('getPublicPathPost')) {
  function getPublicPathPost()
  {
    $url = url('/');

    if (preg_match('/fixitcenterchainat.com/', $url)) {
      $base = preg_split('/\/laravel/', base_path());
      return $base[0] . '/public_html/upload/posts/';
    }

    return public_path('upload/posts/');
  }
}

if (! function_exists('getPublicPathName')) {
  function getPublicPathName($name)
  {
    $url = url('/');
    $path = 'upload/' . $name . '/';

    if (preg_match('/fixitcenterchainat.com/', $url)) {
      $base = preg_split('/\/laravel/', base_path());
      return $base[0] . '/public_html/' . $path . '/';
    }

    return public_path($path);
  }
}

if (! function_exists('getPublicPathCitizen')) {
  function getPublicPathCitizen()
  {
    $url = url('/');

    if (preg_match('/fixitcenterchainat.com/', $url)) {
      $base = preg_split('/\/laravel/', base_path());
      return $base[0] . '/public_html/upload/citizens/';
    }
    return public_path('upload/citizens/');
  }
}
