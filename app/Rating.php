<?php

namespace App;

use App\Rating;
use Illuminate\Database\Eloquent\Model;

class Rating extends Model
{
  protected $fillable = [
    'id', 'sex', 'age', 'career', 'education',
    'option1', 'option2', 'option3', 'option4', 'option5', 'option6', 'option7',
    'sum_option'
  ];

  public function whereCount($q, $key, $value)
  {
    return $q->where($key, $value)->count();
  }

  public static function getStats($key)
  {
    $distinct = Rating::distinct($key)->count($key);
    $peoples = Rating::count();
    $array = array();

    for ($i = 1; $i <= $distinct; $i++) {
      $count = Rating::where($key, $i)->count();
      $percent = round(($count / $peoples) * 100.0, 2);
      $array[$i] = array("count" => $count, "percent" => $percent);
    }

    return $array;
  }

  public static function ratingDesription($avg)
  {
    if ($avg >= 4.5) return 'มากที่สุด';
    if ($avg >= 3.5) return 'มาก';
    if ($avg >= 2.5) return 'ปานกลาง';
    if ($avg >= 1.5) return 'น้อย';
    return 'น้อยที่สุด';
  }

  public static function ratingStats()
  {
    $option_key = array();
    for($i = 1; $i <= 7; $i++) array_push($option_key, "option".$i);

    $options = array();
    foreach ($option_key as $value) {
      $avg = round(Rating::avg($value),2);
      $description = Rating::ratingDesription($avg);
      $options[$value] = array("avg" => $avg, "description" => $description);
    }

    $avg = round(collect($options)->avg('avg'), 2);
    $options['summary'] = array('avg' => $avg, "description" => Rating::ratingDesription($avg));

    return $options;
  }
}
