<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Richtext extends Model
{
  protected $fillable = ['content', 'name', 'description'];
}
