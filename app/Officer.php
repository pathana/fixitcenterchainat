<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Officer extends Model
{
  protected $fillable = [
    'fullname', 'citizen_number',
    'avatar', 'phonenumber',
    'type', 'department_id'
  ];

  public function department()
  {
    return $this->belongsTo(Department::class);
  }

  public function peoples()
  {
    return $this->hasMany(People::class);
  }

  public function getTypeNameAttribute()
  {
    $name = [
      'student' => 'นักศึกษา',
      'teacher' => 'ครูอาจารย์่',
      'officer' => 'เจ้าหน้าที่',
      'chief' => 'ผู้บริหาร',
      'community' => 'ชุมชน'
    ];

    return $name[$this->type];
  }

  public function getNumberFormatAttribute()
  {
    $pattern = '_-____-_____-__-_';
    return $this->numberformat($pattern, '-', $this->citizen_number);
  }

  public function getPhoneFormatAttribute()
  {
    $pattern = '___-_______';
    return $this->numberformat($pattern, '-', $this->phonenumber);
  }

  public function numberformat($pattern, $ex, $text)
  {
    $patterns = explode('-', $pattern);
    $first = 0;
    $last = 0;

    for ($i = 0; $i < count($patterns); $i++) {
      $first = $first + $last;
      $last = strlen($patterns[$i]);
      $returnText[] = substr($text, $first, $last);
    }

    return implode($ex, $returnText);
  }

  public static function null_data()
  {
    return [
      'fullname' => '-',
      'citizen_number' => '-',
      'type' => '-',
      'department' => '-',
    ];
  }
}
