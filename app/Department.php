<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
  protected $fillable = ['name', 'aka'];

  public function officers()
  {
    return $this->hasMany(Officer::class);
  }

  public function spare_parts()
  {
    return $this->hasMany(SparePart::class);
  }

}
