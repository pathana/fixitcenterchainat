<?php

namespace App;

use App\ItemSuggestion;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\QueryException;

class ItemSuggestion extends Model
{
  protected $fillable = ['name'];

  public static function add_name(string $names)
  {
    if (empty($names)) return;
    $names = explode(",", $names);

    foreach ($names as $value) {
      try {
        ItemSuggestion::create([
          'name' => $value
        ]);
      }
      catch (QueryException $e) {
        $error_code = $e->errorInfo[1];
        if ($error_code == 1062) continue;
      }
    }
  }
}
