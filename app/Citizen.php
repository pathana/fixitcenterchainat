<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Citizen extends Model
{
  protected $fillable = [
    'title',
    'fullname',
    'address',
    'number',
    'phonenumber',
    'avatar'
  ];

  public function repairs()
  {
    return $this->hasMany(Repair::class);
  }

  public function peoples()
  {
    return $this->hasMany(People::class);
  }

  public function getNumberFormatAttribute()
  {
    $pattern = '_-____-_____-__-_';
    return $this->numberformat($pattern, '-', $this->number);
  }

  public function getPhoneFormatAttribute()
  {
    $pattern = '___-_______';
    return $this->numberformat($pattern, '-', $this->phonenumber);
  }

  public function numberformat($pattern, $ex, $text)
  {
    $patterns = explode('-', $pattern);
    $first = 0;
    $last = 0;

    for ($i = 0; $i < count($patterns); $i++) {
      $first = $first + $last;
      $last = strlen($patterns[$i]);
      $returnText[] = substr($text, $first, $last);
    }

    return implode($ex, $returnText);
  }
}
