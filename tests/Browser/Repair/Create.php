<?php

namespace Tests\Browser\Repair;

use App\User;
use Faker\Factory;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\WithFaker;

class Create extends DuskTestCase
{
    use withFaker;
     /** @test */
    public function single_create()
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs(User::find(1))
                    ->visit('/center/main/create')
                    ->type('itemName', $this->faker->city)
                    ->radio('itemType', 'vehicle')
                    ->type('itemProblem', $this->faker->city)
                    ->type('itemStatus', $this->faker->city)
                    ->type('itemAttach', $this->faker->city)
                    ->type('itemDescription', $this->faker->city)
                    ->attach('itemPhotos', database_path('photos/1.jpg'))
                    ->attach('itemPhotos', database_path('photos/2.jpg'))
                    ->attach('itemPhotos', database_path('photos/3.jpg'))
                    ->pause(5000);
                    // ->assertSee('Laravel');
        });
    }
}
