<?php

namespace Tests\Browser\Dusk\Auth;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class LoginTest extends DuskTestCase
{
  /** @test */
    public function loginAndClickButton()
    {
      $this->browse(function (Browser $browser) {
        $browser->visit('/login')
                ->type('email', 'admin@fixit.com')
                ->type('password', 'adminfixit')
                ->press('Login')
                ->assertPathIs('/');
      });
    }

  /** @test */
  public function logoutAndClickButton()
  {
    $this->browse(function (Browser $browser) {
      $browser->visit('/admin/dashboard')
              ->visit('/logout')
              ->assertPathIs('/');
    });
  }


}
