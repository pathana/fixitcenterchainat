<?php

namespace Tests\Browser\Dusk\Admin;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Facebook\Webdriver\WebDriver;

class AdvertiseTest extends DuskTestCase
{
  /**
  * @test
  * @group advertise
  */
  public function login()
  {
    $this->browse(function (Browser $browser) {
      $browser->visit('/login')
              ->type('email', 'admin@fixit.com')
              ->type('password', 'adminfixit')
              ->press('Login');
    });
  }

  /**
  * @test
  * @group advertise
  */
  public function create()
  {
    $this->browse(function (Browser $browser) {
      $driver = $browser->driver;
      $browser->visit('/admin/advertise/create')
              ->type('title', 'title');
      $driver->findElement(WebDriver::tagName('textarea'))
            ->sendKeys('textarea');
      $browser->wait(5000);
    });
  }
}
